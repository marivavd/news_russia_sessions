import 'package:flutter/material.dart';

class ModelProduct {
  final String id;
  final String title;
  final String categoryId;
  final double cost;
  final String description;
  final List<String> covers;
  final bool isFavorite;
  final List<Color> colors;
  int count;

  ModelProduct(
      {
        required this.id,
        required this.title,
        required this.categoryId,
        required this.cost,
        required this.description,
        required this.covers,
        required this.isFavorite,
        required this.colors,
        this.count = 1
      }
      );
}
