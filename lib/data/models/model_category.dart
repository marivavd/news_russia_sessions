import 'package:new_sessions/data/models/model_product.dart';

class ModelCategory {
  final String id;
  final String title;

  ModelCategory({required this.id, required this.title});

  List<ModelProduct> getProductsCategory(List<ModelProduct> listProduct){
    return
      listProduct.where(
              (element) => element.categoryId == id).toList();
  }
}
