import 'dart:convert';

import 'dart:convert';

List<String> getOnboardingItems(){
  return [
    jsonEncode({
      'icon': 'assets/image 3.png',
      'titleUp': 'ДОБРО\nПОЖАЛОВАТЬ',
      'titleDown': '',
      'text': '',
    }),
    jsonEncode({
      'icon': 'assets/Spring_prev_ui 1.png',
      'titleDown': 'Начнем\n путешествие',
      'titleUp': '',
      'text': 'Умная, великолепная и модная \nколлекция Изучите сейчас'
    }),
    jsonEncode({
      'icon': 'assets/Aire Jordan Nike.png',
      'titleDown': 'У вас есть сила, \nчтобы',
      'titleUp': '',
      'text': 'В вашей комнате много красивых и \nпривлекательных растений'
    }),
  ];
}
