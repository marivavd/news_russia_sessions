import 'package:new_sessions/data/storage/shared_preference.dart';
import 'package:shared_preferences/shared_preferences.dart';

Future<void> initSharedPreferences()async{
  sharedPreferences = await SharedPreferences.getInstance();
}
String getPassword(){
  return sharedPreferences?.getString('password') ?? '';
}

Future<void> setPassword(String password)async{
  await sharedPreferences?.setString('password', password);
}


List<String> getRequests(){
  return sharedPreferences?.getStringList('requests') ?? [];
}

Future<void> setRequests(String  request)async{
  await sharedPreferences?.setStringList('requests', getRequests() + [request]);
}
