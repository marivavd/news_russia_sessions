
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:new_sessions/data/models/model_category.dart';
import 'package:new_sessions/data/models/model_product.dart';
import 'package:new_sessions/domain/count_cross.dart';
import 'package:new_sessions/domain/make_products_models.dart';
import 'package:new_sessions/main.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

Future<void> signUp(String name, String email, String password)async{
  final AuthResponse res = await supabase.auth.signUp(
    email: email,
    password: password,
    data: {
      'name': name,
      'phone': '',
      'cardName': '',
      'cardNumber': '',
      'avatar': ''
    }
  );

}


Future<void> logOut()async{
  await supabase.auth.signOut();
}


Future<void> editProfile(String name, String email, String password)async{
  await supabase.auth.updateUser(
    UserAttributes(
        email: email,
        data: {
          'name': name,
        }
    )
  );

}
Future<List<String>> getParamentrs()async{
  List<String> result= [];
  result.add(supabase.auth.currentUser!.email!);
  result.add(supabase.auth.currentUser!.userMetadata!['phone']!);
  result.add(supabase.auth.currentUser!.userMetadata!['cardName']!);
  result.add(supabase.auth.currentUser!.userMetadata!['cardNumber']!);
  return result;

}

Future<List<String>> getPhoneAndId()async{
  List<String> result = [];
  result.add(supabase.auth.currentUser!.userMetadata!['phone']!);
  result.add(supabase.auth.currentUser!.id);
  return result;


}

Future<List<String>> getProfile()async{
  List<String> result= [];
  result.add(supabase.auth.currentUser!.userMetadata!['name']!);
  result.add(supabase.auth.currentUser!.email!);
  //result.add(supabase.auth.currentUser!.password!);
  result.add('');

  return result;

}

Future<dynamic> getMyAvatar()async{
  if (supabase.auth.currentUser!.userMetadata!['avatar'] != ''){
    var file = await supabase.storage.from('Avatars').download(supabase.auth.currentUser!.userMetadata!['avatar']);
    return file;
  }
  return null;
}
Future<void> uploadAvatar(Uint8List bytes) async {
  var name = "${supabase.auth.currentUser!.id}.png";
  if (supabase.auth.currentUser!.userMetadata!['avatar'] != ''){
    await supabase.storage
        .from("Avatars")
        .remove(["${supabase.auth.currentUser!.id}.png"]);
  }
  await supabase.storage
      .from("Avatars")
      .uploadBinary(
      name,
      bytes
  );
  await supabase.auth.updateUser(
      UserAttributes(
          data: {
            "avatar": name
          }
      )
  );
}
Future<void> removeAvatar() async {
  await supabase.storage
      .from("Avatars")
      .remove(["${supabase.auth.currentUser!.id}.png"]);
  await supabase.auth.updateUser(
      UserAttributes(
          data: {
            "avatar": null
          }
      )
  );
}


Future<void> loadAllParametrs(String email, String phone, String cardName, String cardNumber,)async{

  await supabase.auth.updateUser(UserAttributes(email: email,
  data: {
    'phone': phone,
    'cardName': cardName,
    'cardNumber': cardNumber
  }));
}


Future<void> signIn(String email, String password)async{
  final AuthResponse res =  await supabase.auth.signInWithPassword(
      email: email,
      password: password,
  );

}

Future<void> sendCode(String email)async{
  await supabase.auth.resetPasswordForEmail(email);

}

Future<void> verifyCode(
    String email,
    String code
    ) async {
  final AuthResponse res = await supabase.auth.verifyOTP(
    type: OtpType.email,
    token: code,
    email: email,
  );

}

Future<void> changePass(
    String password
    ) async {
  await supabase.auth.updateUser(UserAttributes(password: password));
}

@override
Future<void> signInWithGoogle() async {
  const webClientId = '770081476123-vko1apotlb83pvha8oog4tdqtq7fl2aj.apps.googleusercontent.com';
  final GoogleSignIn googleSignIn = GoogleSignIn(
    clientId: webClientId,
  );
  final googleUser = await googleSignIn.signIn();
  final googleAuth = await googleUser!.authentication;
  final accessToken = googleAuth.accessToken;
  final idToken = googleAuth.idToken;

  if (idToken == null) {
    throw 'No ID Token found.';
  }

  await supabase.auth.signInWithIdToken(
    provider: OAuthProvider.google,
    idToken: idToken,
    accessToken: accessToken,
  );
}
Future<List<ModelCategory>> getAllCategories(
    ) async {
  final data = await supabase
      .from('Categories')
      .select();
  List<ModelCategory> result = [];
  for (var val in data){
    result.add(ModelCategory(id: val['id'], title: val['title']));
  }
  return result;
}


Future<List<ModelProduct>> getProductsByCategory(String categoryId) async {
  final data = await supabase
      .from('Products')
      .select().eq('categoryId', categoryId);
  List<ModelProduct> result = await makeProductsModels(data);
  return result;
}
Future<List<ModelProduct>> getProducts() async {
  final data = await supabase
      .from('Products')
      .select();
  List<ModelProduct> result = await makeProductsModels(data);
  return result;
}

Future<String> getImageofProduct(String idProduct, String color)async{
    var url = await supabase.storage.from('ProductImages').getPublicUrl('$idProduct-$color.png');
    return url;
}


Future<List<ModelProduct>> getPopularProducts() async {
  final data = await supabase
      .from('Products')
      .select().eq('is_best_seller', true);
  List<ModelProduct> result = await makeProductsModels(data);

  return result;
}


Future<String> getNameCategory(String id) async {
  final data = await supabase.from('Categories').select().eq('id', id).single();
  return data['title'];
}

Future<bool> getFavourite(String id) async {
  final data = await supabase.from('Favourite').select('id_product').eq('id_user', supabase.auth.currentUser!.id).eq('id_product', id);
  return (data.length != 0)?true:false;
}

Future<bool> getBasked(String id) async {
  final data = await supabase.from('Basket').select('id_product').eq('id_user', supabase.auth.currentUser!.id).eq('id_product', id);
  return (data.length != 0)?true:false;
}


Future<void> deleteFavourite(String id) async {
  final data = await supabase.from('Favourite').delete().match({'id_product': id, 'id_user': supabase.auth.currentUser!.id});
}

Future<void> updateBasked(String id) async {
  final count = await getBaskedCount(id);
  final data = await supabase.from('Basket').update({'count': count + 1}).match({'id_product': id, 'id_user': supabase.auth.currentUser!.id});
}

Future<int> getBaskedCount(String id) async {
  final data = await supabase.from('Basket').select().eq('id_user', supabase.auth.currentUser!.id).eq('id_product', id).single();
  return data['count'];
}

Future<void> putFavourite(String id) async {
  final data = await supabase.from('Favourite').insert({'id_product': id, 'id_user': supabase.auth.currentUser!.id});
}

Future<void> putBasked(String id) async {
  final data = await supabase.from('Basket').insert({'id_product': id, 'id_user': supabase.auth.currentUser!.id, 'count': 1});
}

Future<String> getImageAdvert()async{
  var url = await supabase.storage.from('ProductImages').getPublicUrl('cross.jpg');
  return url;
}


Future<List<ModelProduct>> getAllFavouriteProduucts() async {
  final dataFavourite = await supabase.from('Favourite').select('id_product').eq('id_user', supabase.auth.currentUser!.id);
  List<String> lines = [];
  for (var val in dataFavourite){
    lines.add(val['id_product']);
  }
  List<ModelProduct> result = await getProductsById(lines);
  return result;

}

Future<List<ModelProduct>> getProductsById(List<String> idSp) async {
  final data = await supabase
      .from('Products')
      .select().inFilter('id', idSp);
  List<ModelProduct> result = await makeProductsModels(data);

  return result;
}

Future<ModelProduct> getOneProductById(String id) async {
  final data = await supabase
      .from('Products')
      .select().eq('id', id);
  List<ModelProduct> result = await makeProductsModels(data);

  return result[0];
}


Future<List<ModelProduct>> getAllProductsInBasket() async {
  final data = await supabase.from('Basket').select('id_product, count').eq('id_user', supabase.auth.currentUser!.id);
  List<String> lines = [];
  for (var val in data){
    lines.add(val['id_product']);
  }
  List<ModelProduct> result = await getProductsById(lines);
  countProducts(result, data);
  return result;
}


Future<int> makeOrder(String email, String phone, String cardName, String cardNumber, address,) async {
  final data = await supabase.from('Orders').
  insert({'id_user': supabase.auth.currentUser!.id, 'email': email, 'phone': phone, 'address': address, 'cardName': cardName, 'cardNumber': cardNumber}).select().single();
  return data['id'];
}

Future<void> makeOrderItems(String title, double coast, int count, int idOrder, String idProduct) async {
  final data = await supabase.from('OrdersItems').
  insert({'title': title, 'coast': coast, 'count': count, 'id_order': idOrder, 'uuid_product': idProduct});

}

Future<void> clearBasket() async {
  final data = await supabase.from('Basket').delete().match({'id_user': supabase.auth.currentUser!.id});
}

Future<List<Map<String, dynamic>>> getOrderItems(int idOrder) async {
  final data = await supabase.from('OrdersItems').select()
  .eq('id_order', idOrder);
  for (var val in data){
     ModelProduct model = await getOneProductById(val['uuid_product'].toString());
    val['image'] = model.covers.first;
  }
  return data;

}


Future<List<int>> getOrders() async {
  final data = await supabase.from('Orders').select()
      .eq('id_user', supabase.auth.currentUser!.id).order('created_at', ascending: false);
  List<int> result = [];
  for (var val in data){
    result.add(val['id']);
  }
  return result;

}

Future<List<Map<String, dynamic>>> getNotifications() async {
  final data = await supabase
      .from('Messages')
      .select().eq('id_user', supabase.auth.currentUser!.id);

  return data;
}

void subsribeNews(callback){
  supabase
      .channel("changes")
      .onPostgresChanges(
      event: PostgresChangeEvent.insert,
      schema: "public",
      table: "Messages",
      callback: (payload) {
        callback(payload.newRecord);
      }
  )
      .subscribe();
}
