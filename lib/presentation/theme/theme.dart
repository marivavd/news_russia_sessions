import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

var themeData = ThemeData(
    useMaterial3: true,
    colorScheme: ColorScheme.fromSeed(seedColor: const Color(0xFF48B2E7)),
    textTheme: TextTheme(
        labelMedium: GoogleFonts.poppins(
            textStyle: const TextStyle(
              fontWeight: FontWeight.w500,
              fontSize: 12,
              color: Color(0xFF48B2E7),
              height: 16/12,
            )
        ),
        labelSmall: GoogleFonts.poppins(
            textStyle: const TextStyle(
                fontWeight: FontWeight.w500,
                fontSize: 14,
                height: 16/14,
                color: Color(0xFF2B2B2B)
            )
        ),
        titleMedium: GoogleFonts.raleway(
            textStyle: const TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.w600,
              color: Color(0xFF6A6A6A),
              height: 20/16,
            )
        )
    )
);
