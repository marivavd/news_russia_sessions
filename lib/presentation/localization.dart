import 'package:change_case/change_case.dart';

mixin AppLocale {
  static const String signInTitle = 'signInTitle';
  static const String signUpTitle = 'signUpTitle';
  static const String forgotTitle = 'forgotTitle';
  static const String otpTitle = 'otpTitle';
  static const String email = 'email';
  static const String name = 'name';
  static const String password = 'password';
  static const String signInText = 'signInText';
  static const String signUpText = 'signUpText';
  static const String forgotText = 'forgotText';
  static const String otpText = 'otpText';
  static const String signInButton = 'signInButton';
  static const String signUpButton = 'signUpButton';
  static const String forgotButton = 'forgotButton';
  static const String otpButton = 'otpButton';
  static const String googleUpButton = 'googleUpButton';
  static const String googleInButton = 'googleInButton';
  static const String newUser = 'newUser';
  static const String createAc = 'createAc';
  static const String already = 'already';
  static const String logIn = 'logIn';
  static const String otpCode = 'otpCode';
  static const String resend = 'resend';
  static const String error = 'error';
  static const String recoveryPass = 'recoveryPass';
  static const String pdf = 'pdf';
  static const String rememberPass = 'rememberPass';
  static const String main = 'main';
  static const String search = 'search';
  static const String category = 'category';
  static const String categoryAll = 'categoryAll';
  static const String popular = 'popular';
  static const String sales = 'sales';
  static const String seeAll = 'seeAll';
  static const String readMore = 'readMore';
  static const String toBasket = 'toBasket';
  static const String alreadyBasket = 'alreadyBasket';
  static const String favorite = 'favorite';
  static const String cart = 'cart';
  static const String items = 'items';
  static const String summa = 'summa';
  static const String delivery = 'delivery';
  static const String totalCost = 'totalCost';
  static const String checkout1 = 'checkout1';
  static const String checkout2 = 'checkout2';
  static const String contactInfo = 'contactInfo';
  static const String phone = 'phone';
  static const String watchMap = 'watchMap';
  static const String address = 'address';
  static const String paymentMethod = 'paymentMethod';
  static const String successful = 'successful';
  static const String backToShopping = 'backToShopping';
  static const String orders = 'orders';
  static const String profile = 'profile';
  static const String load = 'load';
  static const String done = 'done';
  static const String firstName = 'firstName';
  static const String lastName = 'lastName';
  static const String location = 'location';
  static const String mobileNumber = 'mobileNumber';
  static const String changeAvatar = 'changeAvatar';
  static const String notifications = 'notifications';
  static const String settings = 'settings';
  static const String signOut = 'signOut';







  static const Map<String, dynamic> RU = {
    signInTitle: 'Привет!',
  signUpTitle: 'Регистрация',
  forgotTitle : 'Забыл пароль',
  otpTitle : 'OTP проверка',
  email : 'Email',
  name : 'Ваше имя',
  password : 'Пароль',
  signInText : 'Заполните Свои Данные Или \nПродолжите Через Социальные Медиа',
  signUpText : 'Заполните Свои Данные Или \nПродолжите Через Социальные Медиа',
  forgotText : 'Введите Свою Учетную Запись\nДля Сброса',
  otpText : 'Пожалуйста, Проверьте Свою \nЭлектронную Почту, Чтобы Увидеть\nКод Подтверждения',
  signInButton : 'Войти',
  signUpButton : 'Зарегистрироватеься',
  forgotButton : 'Отправить',
  googleUpButton : 'Войти С Google',
  googleInButton : 'Войти С Google',
  newUser : 'Вы впервые? ',
  createAc : 'Создать пользователя',
  already : 'Есть аккаунт? ',
  logIn : 'Войти',
  otpCode : 'OTP Код',
  resend : 'Отправить заново',
  error : 'Ошибка',
  recoveryPass : 'Востановить',
  pdf : 'Даю согласие на обработку \nперсональных данных',
    rememberPass: 'Запомнить пароль',
    main: 'Главная',
    search: 'Поиск',
  category: 'Категории',
    categoryAll: 'Все',
    popular: 'Популярное',
    sales: 'Акции',
    seeAll: 'Все',
    readMore: 'Подробнее',
    toBasket: 'В Корзину',
    alreadyBasket: 'В Корзине',
    favorite: 'Избранное',
    cart: 'Корзина',
    items: 'товара',
    summa: 'Сумма',
    delivery: 'Доставка',
    totalCost: 'Итого',
    checkout1: 'Оформить заказ',
    checkout2: 'Подтвердить',
    contactInfo: 'Контактная информация',
    phone: 'Телефон',
    watchMap: 'Посмотреть на карте',
    address: 'Адрес',
    paymentMethod: 'Способ оплаты',
    successful: 'Вы успешно\nоформили заказ',
    backToShopping: 'Вернуться к покупкам',
    orders: 'Заказы',
    profile: "Профиль",
    load: 'Сохранить',
    done: 'Готово',
    changeAvatar: 'Изменить фото профиля',
    firstName: 'Имя',
    lastName: 'Фамилия',
    location: 'Адрес',
    mobileNumber: 'Телефон',
    notifications: 'Уведомления',
    settings: 'Настройки',
    signOut: 'Выйти'




};

  static const Map<String, dynamic> EN = {
    signInTitle: 'Hello Again!',
    signUpTitle: 'Register Account',
    forgotTitle : 'Forgot Password',
    otpTitle : 'OTP Verification',
    email : 'Email Address',
    name : 'Your Name',
    password : 'Password',
    signInText : 'Fill Your Details Or Continue With\nSocial Media',
    signUpText : 'Fill Your Details Or Continue With\nSocial Media',
    forgotText : 'Enter Your Email Account To Reset\nYour Password',
    otpText : 'Please Check Your Email To See The\тVerification Code',
    signInButton : 'Sign In',
    signUpButton : 'Sign up',
    forgotButton : 'Reset password',
    googleUpButton : 'Sign up with Google ',
    googleInButton : 'Sign In with Google ',
    newUser : 'New User? ',
    createAc : 'Create Account',
    already : 'Already Have Account? ',
    logIn : 'Log In',
    otpCode : 'OTP Code',
    resend : 'Resend code to',
    error : 'Error',
    recoveryPass : 'Recovery Password',
    pdf : 'I give my consent to the\nprocessing of personal data',
    rememberPass: 'Remember password',
    main: 'Explore',
    search: 'Looking for shoes',
    category: 'Select Category',
    categoryAll: 'All Shoes',
    popular: 'Popular shoes',
    sales: 'New Arrivals',
    seeAll: 'See all',
    readMore: 'Read More',
    toBasket: 'Add to Cart',
    alreadyBasket: 'Already in cart',
    favorite: 'Favourite',
    cart: 'My Cart',
    items: "Item",
    summa: 'Subtotal',
    delivery: 'Delivery',
    totalCost: 'Total Cost',
    checkout1: 'Checkout',
    checkout2: 'Checkout',
    contactInfo: 'Contact Information',
    phone: 'Phone',
    watchMap: 'View Map',
    address: 'Address',
    paymentMethod: 'Payment Method',
    successful: 'Your Payment Is Successful',
    backToShopping: 'Back To Shopping',
    orders: 'Notifications',
    profile: 'Profile',
    load: 'Save Now',
    done: 'Done',
    changeAvatar: 'Change Profile Picture',
    firstName: 'First Name',
    lastName: 'Last Name',
    location: 'Location',
    mobileNumber: 'Mobile Number',
    notifications: 'Notifications',
    settings: 'Settings',
    signOut: 'Sign Out'
  };

}