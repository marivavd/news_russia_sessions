import 'package:flutter/material.dart';
import 'package:new_sessions/domain/otp_use_case.dart';
import 'package:new_sessions/presentation/pages/sign_in.dart';
import 'package:new_sessions/presentation/widgets/generation_dialog.dart';

Future<void> showError(BuildContext context, String error)async{
  await showDialog(context: context,
      builder: (_) => AlertDialog(
        content: Text(error),
        title: Text('Error'),
        actions: [
          TextButton(onPressed: (){Navigator.of(context).pop();}, child: Text('Ok'))
        ],
      ));
}
void hideLoading(BuildContext context){
  Navigator.of(context).pop();
}
void showLoading(BuildContext context){
  showDialog(context: context,
      barrierDismissible: false,
      builder: (_) => PopScope(
        canPop: false,
          child: Dialog(
            backgroundColor: Colors.transparent,
            surfaceTintColor: Colors.transparent,
            child: Center(child: CircularProgressIndicator(),),
          )
      )
  );
}
String? newPassword;

void showGenerationPasswordDialog(BuildContext context, OTPUseCase useCase){
  showDialog(
      context: context,
      builder: (_) => GenerationPasswordDialog(onConfirm: (text){
        useCase.preparePassword(
            text,
                (password) {
              newPassword = password;
              Navigator.pop(context);
              showLoading(context);
              useCase.setNewPassword(
                  newPassword!,
                      (_) async {
                    hideLoading(context);
                    await showNewPassword(context);
                    Navigator.pushAndRemoveUntil(
                        context, MaterialPageRoute(builder: (_) => const SignIn()) , (route) => false);
                  },
                      (error) async{
                    hideLoading(context);
                    await showError(context, error);
                  }
              );
            },
                (error) => showError(context, error)
        );
      })
  );
}

Future<void> showNewPassword(BuildContext context) async {
  await showDialog(context: context, builder: (_) => AlertDialog(
    title: const Text("Ваш новый пароль"),
    content: SelectableText(newPassword!),
  ));
}

