
import 'package:flutter/material.dart';
import 'package:pdfx/pdfx.dart';

class WatchPdf extends StatefulWidget {
  WatchPdf({super.key, required this.pdfController});
  final pdfController;

  @override
  State<WatchPdf> createState() => _WatchPdfState();
}

class _WatchPdfState extends State<WatchPdf> {
  @override
  Widget build(BuildContext context) {
    return PdfViewPinch(controller: widget.pdfController);
  }
}
