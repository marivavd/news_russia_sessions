import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:yandex_mapkit/yandex_mapkit.dart';

class MapPage extends StatefulWidget {
  MapPage({super.key, required this.latitude, required this.longitude});
  double latitude;
  double longitude;

  @override
  State<MapPage> createState() => _MapPageState();
}

class _MapPageState extends State<MapPage> {
  CameraPosition? _userLocation;
  late final YandexMapController _mapController;
  var _mapZoom = 0.0;
  Completer<YandexMapController> _completer = Completer();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFF7F7F9),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
      Padding(
      padding: EdgeInsets.only(top: 48, left: 20, right: 20),child: InkWell(
            child: Container(
                height: 44,
                width: 44,
                decoration: BoxDecoration(
                    shape: BoxShape.rectangle,
                    borderRadius: BorderRadius.circular(40),
                    color: Colors.white
                ),
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 19.25, vertical: 16.25),
                  child: SvgPicture.asset('assets/back.svg', width: 5.5, height: 11.5,),
                )
            ),
            onTap: (){
              Navigator.of(context).pop();
            },
          ),),
          SizedBox(height: 20,),
          SizedBox(
            height: 500,
            child: YandexMap(
              onMapCreated: (controller) async {
                _mapController = controller;

                await _mapController.toggleUserLayer(visible: true);
                _completer.complete(controller);
              },
              onUserLocationAdded: (view) async {
                _userLocation = await _mapController.getUserCameraPosition();
                if (_userLocation != null) {
                  await _mapController.moveCamera(
                    CameraUpdate.newCameraPosition(
                      _userLocation!.copyWith(zoom: 10),
                    ),
                    animation: const MapAnimation(
                      type: MapAnimationType.linear,
                      duration: 0.1,
                    ),
                  );
                }

                return view.copyWith(
                    pin: PlacemarkMapObject(
                        mapId: MapObjectId('My location'),
                        point: Point(latitude: widget.latitude, longitude: widget.longitude),
                        opacity: 1,
                        icon: PlacemarkIcon.single(
                            PlacemarkIconStyle(
                              scale: 1,
                              image: BitmapDescriptor.fromAssetImage(
                                  "assets/blue.png"
                              ),

                            )
                        ))
                );
              },
            ),
          )
        ],
      )
    );
  }
}
