

import 'package:flutter/material.dart';
import 'package:flutter_localization/flutter_localization.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:new_sessions/data/models/model_category.dart';
import 'package:new_sessions/data/models/model_product.dart';
import 'package:new_sessions/domain/listning_use_case.dart';
import 'package:new_sessions/presentation/localization.dart';
import 'package:new_sessions/presentation/utils/dialogs.dart';
import 'package:new_sessions/presentation/widgets/card_product.dart';

import '../../domain/basket_use_case.dart';
import '../../domain/favorite_use_case.dart';

class Favourite extends StatefulWidget {
  Favourite({super.key});



  @override
  State<Favourite> createState() => _FavouriteState();
}

class _FavouriteState extends State<Favourite> {
  BasketUseCase basketUseCase = BasketUseCase();
  FavouriteUseCase favouriteUseCase = FavouriteUseCase();
  // List<bool> isFavourites = [];
  List<bool> isBasket = [];
  List<ModelProduct> products = [];
  bool flag = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async{
      await favouriteUseCase.getAllFavourites(
              (response) {
            products = response;
            setState(() {

            });

          },
              (String e) async{
            await showError(context, e);
          });
      for (var val in products){
        await basketUseCase.isInBasket(
            val.id,
                (response){
              isBasket.add(response);
            },
                (String error)async{
              await showError(context, error);
            }
        );
      }
      flag = true;
      setState(() {

      });
    });


  }



  @override
  Widget build(BuildContext context) {
    return (flag)?Scaffold(
      backgroundColor: Color(0xFFF7F7F9),
      body: Padding(
        padding: EdgeInsets.only(top: 48, left: 20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Align(
                    alignment: Alignment.topLeft,
                    child: InkWell(
                      child: Container(
                          height: 44,
                          width: 44,
                          decoration: BoxDecoration(
                              shape: BoxShape.rectangle,
                              borderRadius: BorderRadius.circular(40),
                              color: Colors.white
                          ),
                          child: Padding(
                            padding: EdgeInsets.symmetric(horizontal: 19.25, vertical: 16.25),
                            child: SvgPicture.asset('assets/back.svg', width: 5.5, height: 11.5,),
                          )
                      ),
                      onTap: (){

                      },
                    )
                ),
                Center(
                    child:
                    Align(
                      alignment: Alignment.topCenter,
                      child: Padding(
                        padding: EdgeInsets.only(top: 6),
                        child: Text(
                            AppLocale.favorite.getString(context),
                            style: GoogleFonts.raleway(
                                textStyle: TextStyle(
                                    fontWeight: FontWeight.w600,
                                    color: Color(0xFF2B2B2B),
                                    height: 20/16,
                                    fontSize: 16)
                            )
                        ),
                      ),
                    )

                ),
                Container(
                    height: 44,
                    width: 44,
                    decoration: BoxDecoration(
                        shape: BoxShape.rectangle,
                        borderRadius: BorderRadius.circular(40),
                        color: Colors.white
                    ),
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                      child: SvgPicture.asset('assets/heart_no.svg', width: 18, height: 16,),
                    )
                ),

              ],
            ),
            SizedBox(height: 21  ,),
            Expanded(child: Padding(
              padding: EdgeInsets.only(right: 20),
              child: GridView.builder(
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2,
                      childAspectRatio: 160/184,
                      mainAxisSpacing: 21,
                      crossAxisSpacing: MediaQuery.of(context).size.width - 40 - 320
                  ),
                  itemCount: products.length,
                  itemBuilder: (_, index) {
                    return CardItem(product:products[index],
                      isBasket: isBasket[index],
                      isFavourite: true,
                      ontapFavourite: ()async{
                        showLoading(context);

                          await favouriteUseCase.delFavourite(
                              products[index].id,
                                  (_){
                                products.remove(products[index]);
                                hideLoading(context);
                                setState(() {

                                });
                              },
                                  (String e)async{
                                hideLoading(context);
                                showError(context, e);
                              }
                          );
                      },
                      onTapBasket: ()async{
                        showLoading(context);
                        if (isBasket[index]){
                          await basketUseCase.updateBasket(
                              products[index].id,
                                  (_){
                                isBasket[index] = true;
                                hideLoading(context);
                                setState(() {

                                });
                              },
                                  (String e)async{
                                hideLoading(context);
                                showError(context, e);
                              }
                          );
                        }
                        else{
                          await basketUseCase.putInBasket(
                              products[index].id,
                                  (_){
                                isBasket[index] = true;
                                hideLoading(context);
                                setState(() {

                                });
                              },
                                  (String e)async{
                                hideLoading(context);
                                showError(context, e);
                              }
                          );
                        }
                      },);
                  }
              ),
            )),
          ],
        ),
      ),



    ):Center(child: CircularProgressIndicator(),);
  }
}
