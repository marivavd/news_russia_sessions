import 'package:flutter/material.dart';
import 'package:flutter_localization/flutter_localization.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:new_sessions/data/models/model_product.dart';
import 'package:new_sessions/domain/search_results.dart';
import 'package:new_sessions/domain/search_use_case.dart';
import 'package:new_sessions/domain/shared_requests.dart';
import 'package:new_sessions/presentation/localization.dart';
import 'package:new_sessions/presentation/pages/details.dart';
import 'package:new_sessions/presentation/utils/dialogs.dart';

class Search extends StatefulWidget {
  const Search({super.key});

  @override
  State<Search> createState() => _SearchState();
}

class _SearchState extends State<Search> {
  TextEditingController controller = TextEditingController();
  List<ModelProduct> results = [];
  List<ModelProduct> allProducts = [];
  SearchUseCase useCase = SearchUseCase();
  List<String> lastRequests = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    lastRequests = getLastRequests();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async{
      await useCase.makeModelsProducts(
              (response) {
            allProducts = response;
            setState(() {

            });

          },
              (String e) async {
                await showError(context, e);
              });
    });
  }
  @override
  Widget build(BuildContext context) {
    return (allProducts.length != 0)?Scaffold(
      backgroundColor: Color(0xFFF7F7F9),
      body: Padding(
          padding: EdgeInsets.symmetric(horizontal: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 48,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    height: 44,
                    width: 44,
                    child: Padding(
                      padding: EdgeInsets.symmetric(vertical: 15, horizontal: 15),
                      child: InkWell(
                        onTap: (){
                          Navigator.of(context).pop();
                        },
                        child: SvgPicture.asset('assets/back.svg'),
                      )
                    ),
                  ),
                  Padding(padding: EdgeInsets.symmetric(vertical: 12),
                  child: Text(
                    AppLocale.search.getString(context),
                    style: GoogleFonts.raleway(
                      textStyle: TextStyle(
                        fontSize: 20,
                        color: Color(0xFF2B2B2B),
                        fontWeight: FontWeight.w600
                      )
                    ),
                  ),),
                  Container(height: 44, width: 44,)
                ],
              ),
              SizedBox(height: 26,),
              Container(
                height: 52,
                width: double.infinity,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(14)
                ),
                child: TextField(
                  controller: controller,
                  textAlign: TextAlign.start,
                  textAlignVertical: TextAlignVertical.center,
                  onChanged: (text){
                    results = searchResults(controller.text, allProducts);
                    setState(() {

                    });
                  },
                  decoration: InputDecoration(
                      hintText: AppLocale.search.getString(context),
                      hintStyle: GoogleFonts.poppins(
                          textStyle: TextStyle(
                            color: Color(0xFF6A6A6A),
                            fontSize: 12,
                            height: 20/12,
                          )
                      ),
                    prefixIcon:InkWell(
                        onTap:()async{

                        },
                        child: SvgPicture.asset('assets/search.svg', color: Color(0xFF6A6A6A)),

                    ),
                    suffixIcon:

                          SvgPicture.asset('assets/microphone.svg'),


                      border: InputBorder.none,
                    suffixIconConstraints: BoxConstraints(minWidth:  26),
                    contentPadding: EdgeInsets.symmetric(vertical: 14, horizontal: 50),
                    prefixIconConstraints: BoxConstraints(minWidth: 24),
                  ),

                ),
              ),
              //SizedBox(height: 26,),
              (controller.text != '')?
              Expanded(
                  child: ListView.separated(
                      shrinkWrap: true,
                  itemBuilder: (_, index){
                    return SizedBox(
                      height: 22,
                      child: InkWell(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Text(
                                results[index].title,
                                style: GoogleFonts.raleway(
                                    textStyle: TextStyle(
                                      color: Color(0xFF2B2B2B),
                                      fontSize: 14,
                                      height:16/14,
                                    )
                                )
                            )
                          ],
                        ),
                        onTap: ()async{
                          showLoading(context);
                          await updateRequests(controller.text);
                          hideLoading(context);
                          Navigator.push(context, MaterialPageRoute(builder: (context) => Details(product: results[index])));
                        },
                      ),
                    );
                  },
                  separatorBuilder: (_, index){return SizedBox(height: 16,);},
                  itemCount: results.length)
              ):Expanded(child: ListView.separated(
                  shrinkWrap: true,
                  itemBuilder: (_, index){
                    return SizedBox(
                      height: 22,
                      child:  Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          SvgPicture.asset('assets/clock.svg'),
                          SizedBox(width: 12,),
                          Text(
                              lastRequests[index],
                              style: GoogleFonts.raleway(
                                  textStyle: TextStyle(
                                    color: Color(0xFF2B2B2B),
                                    fontSize: 14,
                                    height:16/14,
                                  )
                              )
                          )
                        ],
                      ),
                    );
                  },
                  separatorBuilder: (_, index){return const SizedBox(height: 16,);},
                  itemCount: lastRequests.length)
              )
            ],
          ),
        ),
    ):Center(child: CircularProgressIndicator(),);
  }
}
