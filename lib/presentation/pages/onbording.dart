import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:new_sessions/domain/queue_use_case.dart';
import 'package:new_sessions/presentation/pages/sign_up.dart';
import 'package:shared_preferences/shared_preferences.dart';

class OnBoarding extends StatefulWidget {
  OnBoarding({super.key});



  @override
  State<OnBoarding> createState() => _OnBoardingState();
}

class _OnBoardingState extends State<OnBoarding> {

  var controller = PageController();




  @override
  Widget build(BuildContext context){

   var currentElement = next();
    var lengthQueue = len();

    return  Scaffold(
        body: Container(
          width: double.maxFinite,
          height: double.maxFinite,
          decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [
                  Color(0xFF48B2E7),
                  Color(0xFF0076B1)
                ],
              )
          ),

          child:  Column(
            children: [
              Expanded(child: PageView.builder(
                  physics: NeverScrollableScrollPhysics(),
                  controller: controller,
                  onPageChanged: (_){
                    setState(() {

                    });
                  },
                  itemBuilder: (_, index){
                    return SingleChildScrollView(child: Padding(padding: EdgeInsets.symmetric(vertical: 30),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          (currentElement.containsKey("titleUp")) ? Padding(
                            padding: const EdgeInsets.only(top: 55, bottom: 121),
                            child: Text(
                              currentElement['titleUp'],
                              textAlign: TextAlign.center,
                              style: const TextStyle(
                                  height: 70/60,
                                  fontFamily: 'Raleway',
                                  fontWeight: FontWeight.w900,
                                  color: Color(0xFFECECEC),
                                  fontSize: 30
                              ),),
                          ) : const SizedBox(),
                          Image.asset(currentElement['icon'], fit:
                          BoxFit.cover, width: double.infinity,),

                          (currentElement.containsKey("titleDown")) ? Padding(
                            padding: const EdgeInsets.only(bottom: 12, top: 24),
                            child: Text(
                              currentElement['titleDown'],
                              textAlign: TextAlign.center,
                              style: const TextStyle(
                                  height: 70/60,
                                  fontFamily: 'Raleway',
                                  fontWeight: FontWeight.w900,
                                  color: Color(0xFFECECEC),
                                  fontSize: 30
                              ),),
                          ) : const SizedBox(),
                          (currentElement.containsKey("text")) ? Text(
                              currentElement['text'],
                              textAlign: TextAlign.center,
                              style: const TextStyle(
                                  height: 48/32,
                                  fontFamily: 'Poppins',
                                  color: Color(0xFFD8D8D8),
                                  fontSize: 16
                              )) : const SizedBox(),
                          // Spacer(),
                          Padding(
                            padding: const EdgeInsets.only(top: 40),
                            child: Row(

                                mainAxisAlignment: MainAxisAlignment.center,
                                children: List.generate(
                                    3,
                                        (index){
                                      return Padding(padding: EdgeInsets.symmetric(horizontal: 10),
                                        child: Container(
                                          width: (2 - index == lengthQueue)?42:28,
                                          height: 5,
                                          decoration: ShapeDecoration(
                                              color: (2 - index == lengthQueue)?Colors.white:Color(0xFF2B6B8B),
                                              shape: StadiumBorder()
                                          ),
                                        ),);
                                    })
                            ),
                          ),
                          // Spacer(),





                        ],
                      ),));
                  }),),

              Padding(padding: EdgeInsets.only(left: 20, right: 20, bottom: 36),
                child: SizedBox(
                  height: 50,
                  width: double.infinity,
                  child: FilledButton(
                    onPressed: ()async{
                      (lengthQueue == 0)?
                      await makeEndQueue():null;
                      (lengthQueue == 0)?
                      Navigator.push(context, MaterialPageRoute(builder: (context) => SignUp())):
                      controller.nextPage(duration: Duration(milliseconds: 300), curve: Curves.easeIn);



                    },
                    style: FilledButton.styleFrom(
                        backgroundColor: Color(0xFFECECEC),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(13)
                        )
                    ),
                    child: Text(
                      (lengthQueue != 2)?'Далее':'Начать',
                      style: TextStyle(
                          color: Color(0xFF2B2B2B),
                          fontSize: 14,
                          fontWeight: FontWeight.w600,
                          height: 16/14,
                          fontFamily: 'Raleway'
                      ),
                    ),
                  ),
                ),),
            ],
          ),

        )
    );

  }
}
