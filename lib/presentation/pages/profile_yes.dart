import 'dart:typed_data';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/material.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localization/flutter_localization.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:image_picker/image_picker.dart';
import 'package:new_sessions/data/repository/request.dart';
import 'package:new_sessions/domain/avatar_use_case.dart';
import 'package:new_sessions/domain/edit_profile_use_case.dart';
import 'package:new_sessions/domain/map_use_case.dart';
import 'package:new_sessions/main.dart';
import 'package:new_sessions/presentation/localization.dart';
import 'package:new_sessions/presentation/pages/forgot_pass.dart';
import 'package:new_sessions/presentation/pages/home.dart';
import 'package:new_sessions/presentation/pages/profile_yes.dart';
import 'package:new_sessions/presentation/pages/strix_code.dart';
import 'package:new_sessions/presentation/utils/dialogs.dart';
import 'package:new_sessions/presentation/widgets/constant_filed.dart';
import 'package:new_sessions/presentation/widgets/custom_field.dart';
import 'package:syncfusion_flutter_barcodes/barcodes.dart';
import 'package:yandex_mapkit/yandex_mapkit.dart';

class Profile extends StatefulWidget {
  Profile({super.key, required this.fullname, required this.avatar});
  String fullname;
  var avatar;

  @override
  State<Profile> createState() => ProfileState();
}

class ProfileState extends State<Profile> {
  String phone = '';
  EditProfileUseCase useCase = EditProfileUseCase();
  late AvatarUseCase avatarUseCase;
  String id = '';
  bool flag = false;
  String address = '';
  MapUseCase mapUseCase = MapUseCase();


  void onPickAvatar(Uint8List bytes){
    setState(() {
      widget.avatar = bytes;
    });
  }

  void onRemoveAvatar() {
    setState(() {
      widget.avatar = null;
    });
  }

  Future<ImageSource?> onChooseSource()async{
    ImageSource? source;
    await showDialog(
        context: context,
        builder: (_) => AlertDialog(
          title: Text('Choose source'),
          actions: [
            TextButton(onPressed: (){
              source = ImageSource.camera;
              Navigator.pop(context);
            }, child: const Text("Camera")),
            TextButton(onPressed: (){
              source = ImageSource.gallery;
              Navigator.pop(context);
            }, child: const Text("Gallery"))
          ],

        )
    );
    return source;

  }


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async {
      await mapUseCase.getCurrentLocation(
              (Position position){
            mapUseCase.getAddress(
                Point(latitude: position.latitude, longitude: position.longitude),
                    (String ad){
                  address = ad;
                  setState(() {

                  });
                },
                    (String e)async{
                  showError(context, e);
                });
          },
              (String e)async{
            showError(context, e);
          });
      await useCase.getPhoneNumber(
              (response) {
            phone = response[0];
            id = response[1];
            flag = true;
            setState(() {

            });
          },
              (String error) async {
            showError(context, error);
          }
      );
    });
    avatarUseCase = AvatarUseCase(onChooseSource: onChooseSource, onPickAvatar: onPickAvatar, onRemoveAvatar: onRemoveAvatar);
    setState(() {

    });
  }
  @override
  Widget build(BuildContext context) {
    return (flag) ? Scaffold(

      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 48,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Align(
                      alignment: Alignment.topLeft,
                      child: BackButton()
                  ),
                  Center(
                      child:
                      Align(
                        alignment: Alignment.topCenter,
                        child: Padding(
                          padding: EdgeInsets.only(top: 6),
                          child: Text(
                              AppLocale.profile.getString(context),
                              style: GoogleFonts.raleway(
                                  textStyle: TextStyle(
                                      fontWeight: FontWeight.w600,
                                      color: Color(0xFF2B2B2B),
                                      height: 20 / 16,
                                      fontSize: 16)
                              )
                          ),
                        ),
                      )

                  ),
                 InkWell(
                   child:  Text(
                       AppLocale.done.getString(context),
                       style: GoogleFonts.raleway(
                           textStyle: TextStyle(
                               fontWeight: FontWeight.w700,
                               color: Color(0xFF48B2E7),
                               height: 16/15,
                               fontSize: 15)
                       )
                   ),
                   onTap: ()async{
                     showLoading(context);
                     await useCase.loadAvatar(widget.avatar,
                         (_){
                       hideLoading(context);
                       Navigator.push(context, MaterialPageRoute(builder: (context) => Home()));

                         },
                         (String error)async{
                       hideLoading(context);
                       showError(context, error);
                         }
                     );
                   },
                 )

                ],
              ),
              SizedBox(height: 38,),
              Align(
                alignment: Alignment.center,
                child: Container(
                    height: 96,
                    width: 96,
                    decoration: BoxDecoration(
                      border: Border.all(color: Colors.transparent, width: 0),

                      //image: DecorationImage(
                      //image:(avatar == null)?AssetImage('assets/avatar.png'):Image.memory(avatar!),
                      // fit: BoxFit.cover)
                    ),
                    child: ClipOval(
                          child: (widget.avatar != null)?Container(
                            decoration: BoxDecoration(
                                border: Border.all(color: Colors.transparent, width: 0),
                                borderRadius: BorderRadius.circular(40)),
                            child: Image.memory(widget.avatar!, fit: BoxFit.cover, height: 96, width: 96,),
                          ):Image.asset('assets/avatar.png', fit: BoxFit.cover,),

                    )

                ),
              ),
              SizedBox(height: 8,),
              Align(
                alignment: Alignment.center,
                child: Text(
                    widget.fullname,
                    style: GoogleFonts.raleway(
                        textStyle: TextStyle(
                            fontWeight: FontWeight.w600,
                            color: Color(0xFF2B2B2B),
                            height: 23 / 20,
                            fontSize: 20)
                    )
                ),
              ),
              SizedBox(height: 8,),
              Align(
                alignment: Alignment.center,
                child: InkWell(
                  child: Text(
                      AppLocale.changeAvatar.getString(context),
                      style: GoogleFonts.raleway(
                          textStyle: TextStyle(
                              fontWeight: FontWeight.w600,
                              color: Color(0xFF48B2E7),
                              height: 16 / 12,
                              fontSize: 16)
                      )
                  ),
                  onTap: (){
                    avatarUseCase.pressButton(((widget.avatar != null) ? true : false));
                  },
                )
              ),
              SizedBox(height: 11,),
             Container(
               width: double.infinity,
               height: 70,
               color: Color(0xFFF7F7F9),
               child: Padding(
                 padding: EdgeInsets.only(left: 8, right: 8, top: 8, bottom: 9),
                 child: Container(
                   decoration: BoxDecoration(
                     color: Colors.white,
                     borderRadius: BorderRadius.circular(8)
                   ),
                   child: Row(
                     children: [
                       // RotatedBox(
                       //   quarterTurns: 1,
                       //   child: RichText(
                       //     text: TextSpan(
                       //       text: 'Открыть',
                       //       style: GoogleFonts.raleway(
                       //         textStyle: TextStyle(
                       //           fontSize: 12,
                       //           height: 22/12,
                       //           fontWeight: FontWeight.w600,
                       //           color: Colors.black
                       //         )
                       //       ),
                       //       children: [
                       //         WidgetSpan(
                       //           child: RotatedBox(quarterTurns: 1),
                       //         )
                       //       ],
                       //     ),
                       //   ),
                       // ),
                       InkWell(
                         child: SfBarcodeGenerator(value: id,
                         ),
                         onTap: (){
                           Navigator.push(context, MaterialPageRoute(builder: (context) => Shtrix(id: id)));
                         },
                       )
                     ],
                   ),
                 )
               ),
             ),
             ConstantField(label: AppLocale.firstName.getString(context), text: widget.fullname.split(' ')[0]),
              SizedBox(height: 16,),
              ConstantField(label: AppLocale.lastName.getString(context), text: (widget.fullname.split(' ').length != 1)?widget.fullname.split(' ')[1]:''),
              SizedBox(height: 16,),
              ConstantField(label: AppLocale.location.getString(context), text:address),
              SizedBox(height: 16,),
              ConstantField(label: AppLocale.mobileNumber.getString(context), text: phone),


            ],
          ),
        ),
      ),
    ) : Center(child: CircularProgressIndicator(),);
  }
}