import 'package:flutter/material.dart';
import 'package:flutter_localization/flutter_localization.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:new_sessions/domain/forgot_use_case.dart';
import 'package:new_sessions/domain/otp_use_case.dart';
import 'package:new_sessions/presentation/localization.dart';
import 'package:new_sessions/presentation/utils/dialogs.dart';
import 'package:new_sessions/presentation/widgets/custom_field.dart';
import 'package:pinput/pinput.dart';

class OTP extends StatefulWidget {
  OTP({super.key, required this.email});
  String email;

  @override
  State<OTP> createState() => _OTPState();
}

class _OTPState extends State<OTP> {
  OTPUseCase useCase = OTPUseCase();
  SendCodeUseCase useCaseRepeat = SendCodeUseCase();
  var controller = TextEditingController();
  bool button = false;
  bool isError = false;
  DateTime? initialTime;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    initialTime = DateTime.now();
  }


  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    var separator = width / 6 - 46;
    return Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 66,),
              Container(
                  height: 44,
                  width: 44,
                  decoration: BoxDecoration(
                      shape: BoxShape.rectangle,
                      borderRadius: BorderRadius.circular(40),
                      color: Color(0xFFF7F7F9)
                  ),
                  child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 19.25, vertical: 16.25),
          child: SvgPicture.asset('assets/back.svg', width: 5.5, height: 11.5,),
        )
              ),
              SizedBox(height: 11,),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    AppLocale.otpTitle.getString(context),
                    textAlign: TextAlign.center,
                    style: GoogleFonts.raleway(
                        textStyle: TextStyle(
                            fontSize: 32,
                            color: Color(0xFF2B2B2B),
                            fontWeight: FontWeight.w700,
                            height: 38/32
                        )
                    ),
                  ),

                ],
              ),
              SizedBox(height: 8,),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    AppLocale.otpText.getString(context),
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        textStyle: TextStyle(
                            fontSize: 16,
                            color: Color(0xFF707B81),
                            height: 24/16
                        )
                    ),
                  )
                ],
              ),
              SizedBox(height: 16,),
              Padding(padding: EdgeInsets.only(left: 14),
                child: Text(
                  AppLocale.otpCode.getString(context),
                  style: GoogleFonts.raleway(
                      textStyle: TextStyle(
                          fontSize: 21,
                          color: Color(0xFF2B2B2B),
                          fontWeight: FontWeight.w600,
                          height: 25/21
                      )
                  ),
                ),),

              SizedBox(height: 16,),
              Align(
                alignment: Alignment.center,
                child: Pinput(
                  length: 6,
                  controller: controller,
                  separatorBuilder: (context) => SizedBox(width: separator,),
                  defaultPinTheme: PinTheme(
                      width: 46,
                      height: 99,
                      decoration: BoxDecoration(
                          color: Color(0xFFF7F7F9),
                          borderRadius: BorderRadius.circular(12),
                          border: Border.all(
                              color: Colors.transparent
                          )
                      )
                  ),
                  focusedPinTheme: PinTheme(
                      width: 46,
                      height: 99,
                      decoration: BoxDecoration(
                          color: Color(0xFFF7F7F9),
                          borderRadius: BorderRadius.circular(12),
                          border: Border.all(
                              color: Color(0xFFF87265),
                            width: 1
                          )
                      )
                  ),
                  submittedPinTheme: (!isError)?PinTheme(
                      width: 46,
                      height: 99,
                      decoration: BoxDecoration(
                          color: Color(0xFFF7F7F9),
                          borderRadius: BorderRadius.circular(12),
                          border: Border.all(
                              color: Colors.transparent
                          )
                      )
                  ): PinTheme(
                      width: 46,
                      height: 99,
                      decoration: BoxDecoration(
                          color: Color(0xFFF7F7F9),
                          borderRadius: BorderRadius.circular(12),
                          border: Border.all(
                              color: Colors.transparent
                          )
                      )
                  ),
                  onChanged: (text)async{
                    setState(() {
                      isError = false;
                      button = text.length == 6;
                    });
                    if (button){
                      showLoading(context);
                      await useCase.verify(
                          widget.email,
                          controller.text,
                              (_){
                            hideLoading(context);
                            showGenerationPasswordDialog(context, useCase);


                          },
                              (String error)async{
                            hideLoading(context);
                            showError(context, error);
                            isError = true;
                            setState(() {

                            });
                          }
                      );
                    }
                  },
                ),
              ),


              SizedBox(height: 10,),
              InkWell(
                child: Text(
                    AppLocale.resend.getString(context),
                  style: GoogleFonts.raleway(
                      textStyle: TextStyle(
                          fontSize: 12,
                          color: Color(0xFF7D848D),
                          fontWeight: FontWeight.w400,
                          height: 14/12
                      )
                  ),
                ),
                onTap: (initialTime!.difference(DateTime.now()).inMilliseconds > 1000)?()async{
                  showLoading(context);
                  await useCaseRepeat.pressButton(
                      widget.email,
                  (_)async{
                    hideLoading(context);
                    Navigator.push(context, MaterialPageRoute(builder: (context) => OTP(email: widget.email)));

                  },
                  (String error)async{
                  hideLoading(context);
                  initialTime = DateTime.now();
                  showError(context, error);

                },
              );
                }:null)


            ],
          ),
        ),
      ),

    );
  }
}
