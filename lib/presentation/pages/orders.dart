import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localization/flutter_localization.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:new_sessions/domain/orders_use_case.dart';
import 'package:new_sessions/presentation/localization.dart';
import 'package:new_sessions/presentation/utils/dialogs.dart';

class Orders extends StatefulWidget {
  const Orders({super.key});

  @override
  State<Orders> createState() => _OrdersState();
}

class _OrdersState extends State<Orders> {
  OrdersUseCase useCase = OrdersUseCase();
  bool flag = false;
  List<Map<String, dynamic>> orders = [];
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp)async {
      await useCase.getIdOrders(
              (sp) async {
                for(var val in sp){
                   await useCase.getOrdersItems(val,
                           (order){
                     for (int i = 0; i < order.length; i++){
                       orders.add(order[i]);
                       print(2);
                       print(orders);
                       setState(() {

                       });
                     }
                           },
                           (String error)async {
                         showError(context, error);
                       }
                   );
                }
                print(1);
                print(orders);
                flag = true;
                setState(() {

                });

              },
              (String error)async {
                showError(context, error);
              });
    });

  }
  @override
  Widget build(BuildContext context) {
    return (flag)?Scaffold(
      backgroundColor: Color(0xFFF7F7F9),
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 48,),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Align(
                    alignment: Alignment.topLeft,
                    child: InkWell(
                      child: Container(
                          height: 44,
                          width: 44,
                          decoration: BoxDecoration(
                              shape: BoxShape.rectangle,
                              borderRadius: BorderRadius.circular(40),
                              color: Colors.white
                          ),
                          child:Padding(
                            padding: EdgeInsets.symmetric(horizontal: 19.25, vertical: 16.25),
                            child: SvgPicture.asset('assets/back.svg', width: 5.5, height: 11.5,),
                          )
                      ),
                      onTap: (){
                        Navigator.of(context).pop();
                      },
                    )
                ),
                Center(
                    child:
                    Align(
                      alignment: Alignment.topCenter,
                      child: Padding(
                        padding: EdgeInsets.only(top: 6),
                        child: Text(
                            AppLocale.orders.getString(context),
                            style: GoogleFonts.raleway(
                                textStyle: TextStyle(
                                    fontWeight: FontWeight.w600,
                                    color: Color(0xFF2B2B2B),
                                    height: 20/16,
                                    fontSize: 16)
                            )
                        ),
                      ),
                    )

                ),
                Container(
                    height: 44,
                    width: 44,

                ),

              ],
            ),
            SizedBox(height: 16,),
            Expanded(child:
            ListView.separated(
              separatorBuilder: (_, index){
               if(index != 0){
                 return SizedBox(height: 10,);
               }
               return SizedBox();
              },
                itemCount: orders.length,
                itemBuilder: (_, index){
                  return GestureDetector(
                    child: Container(
                      width: double.infinity,
                      height: 105,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(8),
                          border: Border.all(color: Colors.transparent)
                      ),
                      child: Row(
                        children: [
                          SizedBox(width: 10,),
                          Container(
                            width: 87,
                            height: 85,
                            decoration: BoxDecoration(
                                color: Color(0xFFF7F7F9),
                                borderRadius: BorderRadius.circular(16),
                                border: Border.all(color: Colors.transparent)
                            ),
                            child: Padding(
                              padding: EdgeInsets.symmetric(vertical: 10),
                              child: CachedNetworkImage(
                                imageUrl: orders[index]['image'],
                              ),
                            ),
                          ),
                          SizedBox(width: 30,),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  '№ ${orders[index]['id_order'].toString()}',
                                  style: GoogleFonts.poppins(
                                      textStyle: TextStyle(
                                          fontSize: 14,
                                          height: 16/14,
                                          //overflow: TextOverflow.ellipsis,
                                          color: Color(0xFF0D6EFD),
                                          fontWeight: FontWeight.w500
                                      )
                                  ),
                                ),
                                SizedBox(height: 7,),
                                Text(
                                  orders[index]['title'],
                                  softWrap: true,
                                  style: GoogleFonts.poppins(
                                      textStyle: TextStyle(
                                          fontSize: 14,
                                          height: 32/28,
                                          //overflow: TextOverflow.ellipsis,
                                          color: Color(0xFF2B2B2B),
                                          fontWeight: FontWeight.w500
                                      )
                                  ),
                                  //overflow: TextOverflow.visible,
                                ),
                                SizedBox(height: 5,),
                                Text(
                                  '₽${orders[index]['coast']}',
                                  style: GoogleFonts.poppins(
                                      textStyle: TextStyle(
                                          fontSize: 14,
                                          height: 20/14,
                                          color: Color(0xFF2B2B2B)
                                      )
                                  ),
                                ),

                              ],
                            ),
                          ),
                          SizedBox(width: 13,)
                        ],
                      ),

                    ),
                  );
                },
                )
            ),

          ],
        ),
      ),
    ):Center(child: CircularProgressIndicator(),);
  }
}
