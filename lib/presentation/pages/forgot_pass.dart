import 'package:flutter/material.dart';
import 'package:flutter_localization/flutter_localization.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:new_sessions/domain/forgot_use_case.dart';
import 'package:new_sessions/presentation/localization.dart';
import 'package:new_sessions/presentation/pages/otp.dart';
import 'package:new_sessions/presentation/utils/dialogs.dart';
import 'package:new_sessions/presentation/widgets/custom_field.dart';

class ForgotPass extends StatefulWidget {
  const ForgotPass({super.key});

  @override
  State<ForgotPass> createState() => _ForgotPassState();
}

class _ForgotPassState extends State<ForgotPass> {
  var email = TextEditingController();
  SendCodeUseCase useCase = SendCodeUseCase();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 66,),
              Container(
                  height: 44,
                  width: 44,
                  decoration: BoxDecoration(
                      shape: BoxShape.rectangle,
                      borderRadius: BorderRadius.circular(40),
                      color: Color(0xFFF7F7F9)
                  ), child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 19.25, vertical: 16.25),
                child: SvgPicture.asset('assets/back.svg', width: 5.5, height: 11.5,),
              )
              ),
              SizedBox(height: 11,),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    AppLocale.forgotTitle.getString(context),
                    textAlign: TextAlign.center,
                    style: GoogleFonts.raleway(
                        textStyle: TextStyle(
                            fontSize: 32,
                            color: Color(0xFF2B2B2B),
                            fontWeight: FontWeight.w700,
                            height: 38/32
                        )
                    ),
                  ),

                ],
              ),
              SizedBox(height: 8,),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    AppLocale.forgotText.getString(context),
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        textStyle: TextStyle(
                            fontSize: 16,
                            color: Color(0xFF707B81),
                            height: 24/16
                        )
                    ),
                  )
                ],
              ),
              SizedBox(height: 40,),
              CustomField(label: AppLocale.email.getString(context), hint: 'xyz@gmail.com', controller: email),
              SizedBox(height: 40,),

              Align(
                alignment: Alignment.center,
                child: SizedBox(
                  height: 50,
                  width: double.infinity,
                  child: FilledButton(
                    onPressed: ()async{
                      showLoading(context);
                      await useCase.pressButton(email.text,
                              (_)async{
                            hideLoading(context);
                            Navigator.push(context, MaterialPageRoute(builder: (context) => OTP(email: email.text)));

                          },
                              (String error)async{
                            hideLoading(context);
                            showError(context, error);
                          }
                      );

                    },
                    child: Text(
                      AppLocale.forgotButton.getString(context),
                      style: GoogleFonts.raleway(
                          textStyle: TextStyle(
                              fontSize: 14,
                              color: Colors.white,
                              fontWeight: FontWeight.w600,
                              height: 22/14
                          )
                      ),
                    ),
                    style: FilledButton.styleFrom(
                        backgroundColor: Color(0xFF48B2E7),
                        disabledBackgroundColor: Color(0xFF48B2E7),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(13)
                        )
                    ),
                  ),
                ),
              ),


            ],
          ),
        ),
      ),
    );
  }
}
