import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:new_sessions/presentation/pages/favourite.dart';
import 'package:new_sessions/presentation/pages/holder.dart';
import 'package:new_sessions/presentation/pages/main_page.dart';
import 'package:new_sessions/presentation/pages/notifications.dart';
import 'package:new_sessions/presentation/pages/profile.dart';

class Home extends StatefulWidget {
  const Home({super.key});

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  var pages = [MainPage(), Favourite(), Notifications(), ProfileEdit()];
  int currentIndex = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color(0xFFF7F7F9),
        body: Stack(
          children: [
            pages[currentIndex],
            Align(
                alignment: Alignment.bottomCenter,
                child: BottomNavigationBar(
                  backgroundColor: Colors.white,
                  onTap: (val){
                    setState(() {
                      currentIndex = val;
                    });
                  },
                  currentIndex: currentIndex,
                  type: BottomNavigationBarType.fixed,
                  showSelectedLabels: true,
                  showUnselectedLabels: true,
                  selectedFontSize: 12,
                  selectedItemColor: Color(0xFF48B2E7),
                  unselectedFontSize: 12,
                  iconSize: 24,
                  unselectedItemColor: Color(0xFF707B81),

                  items: [
                    BottomNavigationBarItem(icon: SvgPicture.asset('assets/home.svg', color: (currentIndex == 0)?Color(0xFF48B2E7):Color(0xFF707B81)), label: ''),
                    BottomNavigationBarItem(icon: SvgPicture.asset('assets/favourite.svg', color: (currentIndex == 1)?Color(0xFF48B2E7):Color(0xFF707B81)), label: ''),
                    BottomNavigationBarItem(icon: SvgPicture.asset('assets/notification.svg', color: (currentIndex == 2)?Color(0xFF48B2E7):Color(0xFF707B81)), label: ''),
                    BottomNavigationBarItem(icon: SvgPicture.asset('assets/profile.svg', color: (currentIndex == 3)?Color(0xFF48B2E7):Color(0xFF707B81)), label: ''),

                  ],

                )
            ),
          ],
        )

    );
  }
}
