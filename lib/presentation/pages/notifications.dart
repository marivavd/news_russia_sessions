import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localization/flutter_localization.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:new_sessions/domain/notification_use_case.dart';
import 'package:new_sessions/presentation/localization.dart';
import 'package:new_sessions/presentation/utils/dialogs.dart';

class Notifications extends StatefulWidget {
  const Notifications({super.key});

  @override
  State<Notifications> createState() => _NotificationsState();
}

class _NotificationsState extends State<Notifications> {
  bool flag = false;
  List<Map<String, dynamic>> notifictions = [];
  NotifictionUseCase useCase = NotifictionUseCase();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async{
      await useCase.pressButton((p0){
        notifictions = p0;
        flag = true;
        setState(() {

        });
      } ,(String e)async{
        showError(context, e);
      });
    });
  }
  @override
  Widget build(BuildContext context) {
    return (flag)?Scaffold(
        body: Padding(
            padding: EdgeInsets.symmetric(horizontal: 20),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: 48,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Align(
                          alignment: Alignment.topLeft,
                          child: InkWell(
                            child: Container(
                                height: 44,
                                width: 44,
                                decoration: BoxDecoration(
                                    shape: BoxShape.rectangle,
                                    borderRadius: BorderRadius.circular(40),
                                    color: Colors.white
                                ),
                                child:Padding(
                                  padding: EdgeInsets.symmetric(horizontal: 19.25, vertical: 16.25),
                                  child: InkWell(
                                    child: SvgPicture.asset('assets/back.svg', width: 5.5, height: 11.5,),
                                    onTap: (){
                                      Navigator.of(context).pop();
                                    },
                                  )
                                )
                            ),
                            onTap: (){
                              Navigator.of(context).pop();
                            },
                          )
                      ),
                      Center(
                          child:
                          Align(
                            alignment: Alignment.topCenter,
                            child: Padding(
                              padding: EdgeInsets.only(top: 6),
                              child: Text(
                                  AppLocale.notifications.getString(context),
                                  style: GoogleFonts.raleway(
                                      textStyle: TextStyle(
                                          fontWeight: FontWeight.w600,
                                          color: Color(0xFF2B2B2B),
                                          height: 20/16,
                                          fontSize: 16)
                                  )
                              ),
                            ),
                          )

                      ),
                      Container(
                        height: 44,
                        width: 44,

                      ),

                    ],
                  ),
                  SizedBox(height: 28,),
            Expanded(child: ListView.separated(
              separatorBuilder: (_, index){
                if(index != 0){
                  return SizedBox(height: 10,);
                }
                return SizedBox();
              },
              itemCount: notifictions.length,
              itemBuilder: (_, index){
                return GestureDetector(
                  child: Container(
                    width: double.infinity,
                    height: 142,
                    decoration: BoxDecoration(
                        color: Color(0xFFF7F7F9),
                        borderRadius: BorderRadius.circular(16),
                        border: Border.all(color: Colors.transparent)
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(notifictions[index]['title']),
                        Text(notifictions[index]['text']),
                      ],
                    ),


                  ),
                );
              },
            )
        ),

                ]))
    ):Center(child: CircularProgressIndicator(),);
  }
}
