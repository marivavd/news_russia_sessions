import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localization/flutter_localization.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:new_sessions/data/models/model_product.dart';
import 'package:new_sessions/domain/basket_use_case.dart';
import 'package:new_sessions/domain/count_cross.dart';
import 'package:new_sessions/presentation/localization.dart';
import 'package:new_sessions/presentation/pages/checkout.dart';
import 'package:new_sessions/presentation/utils/dialogs.dart';

class Cart extends StatefulWidget {
  const Cart({super.key});

  @override
  State<Cart> createState() => _CartState();
}

class _CartState extends State<Cart> {
  bool flag = false;
  BasketUseCase basketUseCase = BasketUseCase();
  List<ModelProduct> cartProducts = [];
  int countProducts = 0;
  double summa = 0;
  double delivery = 0.0;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async{
      await basketUseCase.getAllCart(
              (response) {
            cartProducts = response;
            countProducts = getCountProducts(cartProducts);
            summa = getSummaProducts(cartProducts);
            flag = true;
            setState(() {

            });

          },
              (String e) async{
            await showError(context, e);
          });
    });

  }
  @override
  Widget build(BuildContext context) {
    return (flag)?Scaffold(
      backgroundColor: Color(0xFFF7F7F9),
      body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
      Padding(
      padding: EdgeInsets.only(top: 48, left: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Align(
                    alignment: Alignment.topLeft,
                    child: InkWell(
                      child: Container(
                          height: 44,
                          width: 44,
                          decoration: BoxDecoration(
                              shape: BoxShape.rectangle,
                              borderRadius: BorderRadius.circular(40),
                              color: Colors.white
                          ),
                          child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: 19.25, vertical: 16.25),
                      child: SvgPicture.asset('assets/back.svg', width: 5.5, height: 11.5,),
                    )
                      ),
                      onTap: (){
                        Navigator.of(context).pop();
                      },
                    )
                ),
                Center(
                    child:
                    Align(
                      alignment: Alignment.topCenter,
                      child: Padding(
                        padding: EdgeInsets.only(top: 6),
                        child: Text(
                            AppLocale.cart.getString(context),
                            style: GoogleFonts.raleway(
                                textStyle: TextStyle(
                                    fontWeight: FontWeight.w600,
                                    color: Color(0xFF2B2B2B),
                                    height: 20/16,
                                    fontSize: 16)
                            )
                        ),
                      ),
                    )

                ),
                Container(
                    height: 44,
                    width: 44,

                ),

              ],
            ),
            SizedBox(height: 16,),
            Text(
              "${countProducts.toString()} ${AppLocale.items.getString(context)}",
                textAlign: TextAlign.start,
                style: GoogleFonts.raleway(
                    textStyle: TextStyle(
                        color: Color(0xFF2B2B2B),
                        height: 24/16,
                        fontSize: 16)
                )
            ),
            SizedBox(height: 8,),])
      ),

            Expanded(child:
            ListView.separated(
                itemBuilder: (_, index){
                  return Padding(padding: EdgeInsets.symmetric(horizontal: 20),
                    child: GestureDetector(
                      child: Container(
                        width: double.infinity,
                        height: 104,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(8),
                            border: Border.all(color: Colors.transparent)
                        ),
                        child: Row(
                          children: [
                            SizedBox(width: 10,),
                            Container(
                              width: 87,
                              height: 85,
                              decoration: BoxDecoration(
                                  color: Color(0xFFF7F7F9),
                                  borderRadius: BorderRadius.circular(16),
                                  border: Border.all(color: Colors.transparent)
                              ),
                              child: Padding(
                                padding: EdgeInsets.symmetric(vertical: 14),
                                child: CachedNetworkImage(
                                  imageUrl: cartProducts[index].covers.first,
                                ),
                              ),
                            ),
                            SizedBox(width: 30,),
                            Expanded(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    cartProducts[index].title,
                                    // softWrap: true,
                                    style: GoogleFonts.poppins(
                                        textStyle: TextStyle(
                                            fontSize: 16,
                                            height: 19/16,
                                            overflow: TextOverflow.ellipsis,
                                            color: Color(0xFF1A2530),
                                            fontWeight: FontWeight.w500
                                        )
                                    ),
                                    //overflow: TextOverflow.visible,
                                  ),
                                  SizedBox(height: 6,),
                                  Text(
                                    '₽${cartProducts[index].cost}',
                                    style: GoogleFonts.poppins(
                                        textStyle: TextStyle(
                                            fontSize: 14,
                                            height: 21/14,
                                            color: Color(0xFF1A2530)
                                        )
                                    ),
                                  ),

                                ],
                              ),
                            ),
                            SizedBox(width: 13,)
                          ],
                        ),

                      ),
                    ));
                },
                separatorBuilder: (_, index){
                  return SizedBox(height: 14,);
                },
                itemCount: cartProducts.length)
            ),

            Align(
              alignment: Alignment.bottomLeft,
              child: Container(
                height: 258,
                width: double.infinity,
                decoration: BoxDecoration(
                  color: Colors.white
                ),
                child: Padding(
                  padding: EdgeInsets.only(left: 20, right: 20, top: 37, bottom: 28),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            AppLocale.summa.getString(context),
                            style: GoogleFonts.raleway(
                              textStyle: TextStyle(
                                color: Color(0xFF707B81),
                                fontSize: 16,
                                height: 20/16,
                                fontWeight: FontWeight.w500
                              )
                            ),
                          ),
                          Text(
                            '₽${summa}',
                            style: GoogleFonts.poppins(
                                textStyle: TextStyle(
                                    color: Color(0xFF2B2B2B),
                                    fontSize: 16,
                                    height: 24/16,

                                )
                            ),
                          ),

                        ],
                      ),
                      SizedBox(height: 8,),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            AppLocale.delivery.getString(context),
                            style: GoogleFonts.raleway(
                                textStyle: TextStyle(
                                    color: Color(0xFF707B81),
                                    fontSize: 16,
                                    height: 20/16,
                                    fontWeight: FontWeight.w500
                                )
                            ),
                          ),
                          Text(
                            '₽${delivery}',
                            style: GoogleFonts.poppins(
                                textStyle: TextStyle(
                                  color: Color(0xFF2B2B2B),
                                  fontSize: 16,
                                  height: 24/16,

                                )
                            ),
                          ),

                        ],
                      ),
                      SizedBox(height: 16,),
                      Divider(height: 1, color: Color(0xFF707B81)),
                      SizedBox(height: 16,),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            AppLocale.totalCost.getString(context),
                            style: GoogleFonts.raleway(
                                textStyle: TextStyle(
                                    color: Color(0xFF2B2B2B),
                                    fontSize: 16,
                                    height: 20/16,
                                    fontWeight: FontWeight.w500
                                )
                            ),
                          ),
                          Text(
                            '₽${delivery + summa}',
                            style: GoogleFonts.poppins(
                                textStyle: TextStyle(
                                  color: Color(0xFF48B2E7),
                                  fontSize: 16,
                                  height: 24/16,

                                )
                            ),
                          ),

                        ],
                      ),
                      SizedBox(height: 30,),
                      Align(
                        alignment: Alignment.center,
                        child: SizedBox(
                          height: 50,
                          width: double.infinity,
                          child: FilledButton(
                            onPressed: (){
                              Navigator.push(context, MaterialPageRoute(builder: (context) => Checkout(allProducts: cartProducts, summa: summa, delivery: delivery)));

                            },
                            child: Text(
                              AppLocale.checkout1.getString(context),
                              style: GoogleFonts.raleway(
                                  textStyle: TextStyle(
                                      fontSize: 14,
                                      color: Colors.white,
                                      fontWeight: FontWeight.w500,
                                      height: 22/14
                                  )
                              ),
                            ),
                            style: FilledButton.styleFrom(
                                backgroundColor: Color(0xFF48B2E7),
                                disabledBackgroundColor: Color(0xFF48B2E7),
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(12)
                                )
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            )

          ],
        ),




    ):Center(child: CircularProgressIndicator(),);
  }
}
