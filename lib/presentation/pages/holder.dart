import 'dart:io';

import 'package:barcode/barcode.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:new_sessions/data/models/model_product.dart';
import 'package:new_sessions/domain/basket_use_case.dart';
import 'package:new_sessions/domain/favorite_use_case.dart';
import 'package:new_sessions/presentation/widgets/card_product.dart';
import 'package:new_sessions/presentation/widgets/card_product_1.dart';
import 'package:new_sessions/presentation/widgets/order_dialog.dart';


class Holder extends StatefulWidget {
  const Holder({super.key});

  @override
  State<Holder> createState() => _HolderState();
}
 Widget buildBarcode(
    Barcode bc,
    String data, {
      String? filename,
      double? width,
      double? height,
      double? fontHeight,
    }) {
  /// Create the Barcode
  final svg = bc.toSvg(
    data,
    width: width ?? 200,
    height: height ?? 80,
    fontHeight: fontHeight,
  );
  return SvgPicture.string(svg);
}

class _HolderState extends State<Holder> {

  BasketUseCase basketUseCase = BasketUseCase();
  FavouriteUseCase favouriteUseCase = FavouriteUseCase();
  // List<bool> isFavourites = [];
  List<bool> isBasket = [];
  List<ModelProduct> products = [];
  bool flag = false;






  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFF7F7F9),
      body: Center(

        child: buildBarcode(
          width: 321,

          Barcode.pdf417(),
          'hjkdjs',
        ),
      )

    );
  }
}
