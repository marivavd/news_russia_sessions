import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_barcodes/barcodes.dart';

class Shtrix extends StatefulWidget {
  Shtrix({super.key, required this.id});
  String id;
  @override
  State<Shtrix> createState() => _ShtrixState();
}

class _ShtrixState extends State<Shtrix> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(

      body: SfBarcodeGenerator(value: widget.id),
    );
  }
}
