import 'package:flutter/material.dart';
import 'package:flutter_localization/flutter_localization.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:new_sessions/data/models/model_category.dart';
import 'package:new_sessions/data/models/model_product.dart';
import 'package:new_sessions/domain/basket_use_case.dart';
import 'package:new_sessions/domain/favorite_use_case.dart';
import 'package:new_sessions/domain/listning_use_case.dart';
import 'package:new_sessions/presentation/localization.dart';
import 'package:new_sessions/presentation/utils/dialogs.dart';
import 'package:new_sessions/presentation/widgets/card_product.dart';

class Listning extends StatefulWidget {
  Listning({super.key, required this.category, required this.categories, required this.index});
  ModelCategory category;
  List<ModelCategory> categories;
  int index;


  @override
  State<Listning> createState() => _ListningState();
}

class _ListningState extends State<Listning> {
  ModelCategory? currentCategory;
  ListningUseCase useCase = ListningUseCase();
  List<ModelProduct> products = [];
  int selectedCategoryIndex = 0;
  BasketUseCase basketUseCase = BasketUseCase();
  FavouriteUseCase favouriteUseCase = FavouriteUseCase();
  List<bool> isFavourites = [];
  List<bool> isBasket = [];
  bool flag = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    currentCategory = widget.category;
    selectedCategoryIndex = widget.index;

    setState(() {

    });
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async{
      await useCase.makeModelsProducts(currentCategory!.id,
              (response) {
        products = response;
        setState(() {

        });

              },
              (String e) async{
        await showError(context, e);
              });
      for (var val in products){
        await basketUseCase.isInBasket(
            val.id,
                (response){
              isBasket.add(response);
            },
                (String error)async{
              await showError(context, error);
            }
        );
        await favouriteUseCase.isInFavourite(
            val.id,
                (response){
              isFavourites.add(response) ;


            },
                (String error)async{
              await showError(context, error);
            }
        );
      }
      flag = true;
      setState(() {

      });
    });


  }


  void refresh(){
    products = [];
    flag = false;
    setState(() {

    });
    currentCategory = widget.categories[selectedCategoryIndex];
    setState(() {

    });
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async {
      await useCase.makeModelsProducts(currentCategory!.id,
              (response) {
            products = response;
            setState(() {

            });
          },
              (String e) async {
            await showError(context, e);
          });
      for (var val in products) {
        await basketUseCase.isInBasket(
            val.id,
                (response) {
              isBasket.add(response);
            },
                (String error) async {
              await showError(context, error);
            }
        );
        await favouriteUseCase.isInFavourite(
            val.id,
                (response) {
              isFavourites.add(response);
            },
                (String error) async {
              await showError(context, error);
            }
        );
      }
      flag = true;
      setState(() {

      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return (products.length != 0 && flag)?Scaffold(
      backgroundColor: Color(0xFFF7F7F9),
      body: Padding(
          padding: EdgeInsets.only(top: 48, left: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Align(
                    alignment: Alignment.topLeft,
                    child: InkWell(
                      child: Container(
                          height: 44,
                          width: 44,
                          decoration: BoxDecoration(
                              shape: BoxShape.rectangle,
                              borderRadius: BorderRadius.circular(40),
                              color: Color(0xFFF7F7F9)
                          ),
                          child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: 19.25, vertical: 16.25),
                      child: SvgPicture.asset('assets/back.svg', width: 5.5, height: 11.5,),
                    )
                      ),
                      onTap: (){
                        Navigator.of(context).pop();
                      },
                    )
                  ),
                 Center(
                    child:
                      Align(
                        alignment: Alignment.topCenter,
                        child: Padding(
                          padding: EdgeInsets.only(top: 6),
                          child: Text(
                              currentCategory!.title,
                              style: GoogleFonts.raleway(
                                  textStyle: TextStyle(
                                      fontWeight: FontWeight.w600,
                                      color: Color(0xFF2B2B2B),
                                      height: 20/16,
                                      fontSize: 16)
                              )
                          ),
                        ),
                      )

                  ),
                  SizedBox(height: 44, width: 44,)
                ],
              ),
              SizedBox(height: 16,),
              Text(
                  AppLocale.category.getString(context),
                  style: GoogleFonts.raleway(
                      textStyle: TextStyle(
                          fontWeight: FontWeight.w600,
                          color: Color(0xFF2B2B2B),
                          height: 19/16,
                          fontSize: 16)
                  )
              ),
              SizedBox(height: 16,),
              SizedBox(height: 40, child: ListView.separated(
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (_, index){
                    var category = widget.categories[index];
                    return Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8),
                          color: (selectedCategoryIndex != index)
                              ? Colors.white
                              : const Color(0xFF48B2E7)
                      ),
                      padding: const EdgeInsets.symmetric(vertical: 11, horizontal: 26),
                      constraints: const BoxConstraints(minWidth: 108),
                      child: GestureDetector(
                          onTap: (){
                            setState(() {
                              selectedCategoryIndex = index;
                            });
                            refresh();
                          },
                          child: Text(
                            category.title,
                            textAlign: TextAlign.center,
                            style: TextStyle(color: ((selectedCategoryIndex == index)
                                ? Colors.white
                                : const Color(0xFF2B2B2B))),
                          )
                      ),
                    );
                  },
                  separatorBuilder: (_, __) => const SizedBox(width: 16),
                  itemCount: widget.categories.length
              )),
              SizedBox(height: 21,),
              Expanded(child: Padding(
                padding: EdgeInsets.only(right: 20),
                child: GridView.builder(
                    // gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    //     mainAxisSpacing: 21,
                    //     crossAxisSpacing: 20,
                    //     crossAxisCount: 2,
                    //     childAspectRatio: 160/184
                    // ),
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                      childAspectRatio: 160/184,
                    mainAxisSpacing: 21,
                    crossAxisSpacing: MediaQuery.of(context).size.width - 40 - 320
                  ),
                    itemCount: products.length,
                    itemBuilder: (_, index) {
                      return CardItem(product: products[index],
                        isBasket: isBasket[index],
                        isFavourite: isFavourites[index],
                        ontapFavourite: ()async{
                          showLoading(context);
                          if (isFavourites[index]){
                            await favouriteUseCase.delFavourite(
                                products[index].id,
                                    (_){
                                  isFavourites[index] = false;
                                  hideLoading(context);
                                  setState(() {

                                  });
                                },
                                    (String e)async{
                                  hideLoading(context);
                                  showError(context, e);
                                }
                            );
                          }
                          else{
                            await favouriteUseCase.putInFavourite(
                                products[index].id,
                                    (_){
                                  isFavourites[index] = true;
                                  hideLoading(context);
                                  setState(() {

                                  });
                                },
                                    (String e)async{
                                  hideLoading(context);
                                  showError(context, e);
                                }
                            );
                          }
                        },
                        onTapBasket: ()async{
                          showLoading(context);
                          if (isBasket[index]){
                            await basketUseCase.updateBasket(
                                products[index].id,
                                    (_){
                                  isBasket[index] = true;
                                  hideLoading(context);
                                  setState(() {

                                  });
                                },
                                    (String e)async{
                                  hideLoading(context);
                                  showError(context, e);
                                }
                            );
                          }
                          else{
                            await basketUseCase.putInBasket(
                                products[index].id,
                                    (_){
                                  isBasket[index] = true;
                                  hideLoading(context);
                                  setState(() {

                                  });
                                },
                                    (String e)async{
                                  hideLoading(context);
                                  showError(context, e);
                                }
                            );
                          }
                        },
                      );
                    }
                ),
              )),
              // Expanded(child: ListView.separated(
              //     itemCount
              //     itemBuilder: itemBuilder,
              //     separatorBuilder: separatorBuilder,
              //     itemCount: ))
              //


            ],
          ),
        ),



    ):Center(child: CircularProgressIndicator(),);
  }
}
