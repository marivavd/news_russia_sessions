import 'package:flutter/material.dart';
import 'package:flutter_localization/flutter_localization.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:new_sessions/domain/edit_profile_use_case.dart';
import 'package:new_sessions/domain/log_out_use_case.dart';
import 'package:new_sessions/presentation/localization.dart';
import 'package:new_sessions/presentation/pages/notifications.dart';
import 'package:new_sessions/presentation/pages/sign_in.dart';
import 'package:new_sessions/presentation/utils/dialogs.dart';

class Menu extends StatefulWidget {
  const Menu({super.key});

  @override
  State<Menu> createState() => _MenuState();
}

class _MenuState extends State<Menu> {
  EditProfileUseCase useCase = EditProfileUseCase();
  var avatar;
  String name = '';
  bool flag = false;
  SignOutUseCase outUseCase = SignOutUseCase();


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async{
      await useCase.getAvatar(
              (response){
            avatar = response;
          },
              (String error)async{
            showError(context, error);
          }
      );
      await useCase.getProfileItems(
              (response) {
            name= response[0];
            flag = true;
            setState(() {

            });
          },
              (String error) async {
            showError(context, error);
          }
      );
    });
  }
  @override
  Widget build(BuildContext context) {
    return (flag)?Scaffold(
      backgroundColor: Color(0xFF48B2E7),
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 28),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 78,),

              Container(
                  height: 96,
                  width: 96,
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.transparent, width: 0),

                    //image: DecorationImage(
                    //image:(avatar == null)?AssetImage('assets/avatar.png'):Image.memory(avatar!),
                    // fit: BoxFit.cover)
                  ),
                  child: ClipOval(
                    child: (avatar != null)?Container(
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.transparent, width: 0),
                          borderRadius: BorderRadius.circular(40)),
                      child: Image.memory(avatar!, fit: BoxFit.cover, height: 96, width: 96,),
                    ):Image.asset('assets/avatar.png', fit: BoxFit.cover,),

                  )

              ),
            SizedBox(height: 15,),
            Text(
              name,
              style: GoogleFonts.raleway(
                textStyle: TextStyle(
                  fontSize: 20,
                  height: 23/20,
                  fontWeight: FontWeight.w700,
                  color: Colors.white
                )
              ),
            ),
            SizedBox(height: 55,),
            InkWell(
              child: Row(
                children: [
                  SvgPicture.asset('assets/profile.svg', color: Colors.white, width: 24, height: 24,),
                  SizedBox(width: 30,),
                  Text(
                    AppLocale.profile.getString(context),
                    style: GoogleFonts.raleway(
                        textStyle: TextStyle(
                            fontSize: 16,
                            height: 20/16,
                            fontWeight: FontWeight.w500,
                            color: Colors.white
                        )
                    ),
                  )
                ],
              ),
              onTap: (){},
            ),
            SizedBox(height: 30,),
            InkWell(
              child: Row(
                children: [
                  SvgPicture.asset('assets/bag.svg', color: Colors.white, width: 24, height: 24,),
                  SizedBox(width: 30,),
                  Text(
                    AppLocale.cart.getString(context),
                    style: GoogleFonts.raleway(
                        textStyle: TextStyle(
                            fontSize: 16,
                            height: 20/16,
                            fontWeight: FontWeight.w500,
                            color: Colors.white
                        )
                    ),
                  )
                ],
              ),
              onTap: (){},
            ),
            SizedBox(height: 30,),
            InkWell(
              child: Row(
                children: [
                  SvgPicture.asset('assets/heart_no.svg', color: Colors.white, width: 24, height: 24,),
                  SizedBox(width: 30,),
                  Text(
                    AppLocale.favorite.getString(context),
                    style: GoogleFonts.raleway(
                        textStyle: TextStyle(
                            fontSize: 16,
                            height: 20/16,
                            fontWeight: FontWeight.w500,
                            color: Colors.white
                        )
                    ),
                  )
                ],
              ),
              onTap: (){},
            ),
            SizedBox(height: 30,),
            InkWell(
              child: Row(
                children: [
                  SvgPicture.asset('assets/orders.svg', color: Colors.white, width: 24, height: 24,),
                  SizedBox(width: 30,),
                  Text(
                    AppLocale.orders.getString(context),
                    style: GoogleFonts.raleway(
                        textStyle: TextStyle(
                            fontSize: 16,
                            height: 20/16,
                            fontWeight: FontWeight.w500,
                            color: Colors.white
                        )
                    ),
                  )
                ],
              ),
              onTap: (){},
            ),
            SizedBox(height: 30,),
            InkWell(
              child: Row(
                children: [
                  SvgPicture.asset('assets/notification.svg', color: Colors.white, width: 24, height: 24,),
                  SizedBox(width: 30,),
                  Text(
                    AppLocale.notifications.getString(context),
                    style: GoogleFonts.raleway(
                        textStyle: TextStyle(
                            fontSize: 16,
                            height: 20/16,
                            fontWeight: FontWeight.w500,
                            color: Colors.white
                        )
                    ),
                  )
                ],
              ),
              onTap: (){
                Navigator.push(context, MaterialPageRoute(builder: (context) =>Notifications()));
              },
            ),
            SizedBox(height: 30,),
            InkWell(
              child: Row(
                children: [
                  SvgPicture.asset('assets/settings.svg', color: Colors.white, width: 24, height: 24,),
                  SizedBox(width: 30,),
                  Text(
                    AppLocale.settings.getString(context),
                    style: GoogleFonts.raleway(
                        textStyle: TextStyle(
                            fontSize: 16,
                            height: 20/16,
                            fontWeight: FontWeight.w500,
                            color: Colors.white
                        )
                    ),
                  )
                ],
              ),
              onTap: (){},
            ),
            SizedBox(height: 38,),
            Divider(height: 1, color: Color(0xFFF7F7F9)),
            SizedBox(height: 30,),
            InkWell(
              child: Row(
                children: [
                  SvgPicture.asset('assets/log_out.svg', color: Colors.white, width: 24, height: 24,),
                  SizedBox(width: 30,),
                  Text(
                    AppLocale.signOut.getString(context),
                    style: GoogleFonts.raleway(
                        textStyle: TextStyle(
                            fontSize: 16,
                            height: 20/16,
                            fontWeight: FontWeight.w500,
                            color: Colors.white
                        )
                    ),
                  )
                ],
              ),
              onTap: ()async{
                showLoading(context);
               await outUseCase.pressButton(
                        (p0){
                          hideLoading(context);
                          Navigator.push(context, MaterialPageRoute(builder: (context) => SignIn()));
                        },
                        (String e)async{
                          hideLoading(context);
                          showError(context, e);
                        }
                );
                },
            )
          ],
        ),
      ),
    ):Center(child: CircularProgressIndicator(),);
  }
}
