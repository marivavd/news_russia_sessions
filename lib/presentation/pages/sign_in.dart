import 'package:flutter/material.dart';
import 'package:flutter_localization/flutter_localization.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:new_sessions/data/repository/shared_preferences.dart';
import 'package:new_sessions/data/storage/shared_preference.dart';
import 'package:new_sessions/domain/load_password.dart';
import 'package:new_sessions/domain/sign_in_use_case.dart';
import 'package:new_sessions/presentation/localization.dart';
import 'package:new_sessions/presentation/pages/forgot_pass.dart';
import 'package:new_sessions/presentation/pages/home.dart';
import 'package:new_sessions/presentation/pages/sign_up.dart';
import 'package:new_sessions/presentation/utils/dialogs.dart';
import 'package:new_sessions/presentation/widgets/custom_field.dart';
import 'package:open_file/open_file.dart';

class SignIn extends StatefulWidget {
  const SignIn({super.key});

  @override
  State<SignIn> createState() => _SignInState();
}

class _SignInState extends State<SignIn> {
  var email = TextEditingController(text: 'mar3@gmail.com');
  var password = TextEditingController();
  bool check = false;
  SignInUseCase useCase = SignInUseCase();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    password.text = getLoadingPassword();
    setState(() {

    });
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 66,),
              Container(
                  height: 44,
                  width: 44,
                  decoration: BoxDecoration(
                      shape: BoxShape.rectangle,
                      borderRadius: BorderRadius.circular(40),
                      color: Color(0xFFF7F7F9)
                  ),
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 19.25, vertical: 16.25),
                    child: SvgPicture.asset('assets/back.svg', width: 5.5, height: 11.5,),
                  )
              ),
              SizedBox(height: 11,),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    AppLocale.signInTitle.getString(context),
                    textAlign: TextAlign.center,
                    style: GoogleFonts.raleway(
                        textStyle: TextStyle(
                            fontSize: 32,
                            color: Color(0xFF2B2B2B),
                            fontWeight: FontWeight.w700,
                            height: 38/32
                        )
                    ),
                  ),

                ],
              ),
              SizedBox(height: 8,),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    AppLocale.signInText.getString(context),
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        textStyle: TextStyle(
                            fontSize: 16,
                            color: Color(0xFF707B81),
                            height: 24/16
                        )
                    ),
                  )
                ],
              ),
              SizedBox(height: 30,),
              CustomField(label: AppLocale.email.getString(context), hint: 'xyz@gmail.com', controller: email),
              SizedBox(height: 30,),
              CustomField(label: AppLocale.password.getString(context), hint: '••••••••', controller: password, enableObscure: true,),
              SizedBox(height: 12,),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  InkWell(
                    child: Text(
                      AppLocale.recoveryPass.getString(context),
                      style: GoogleFonts.raleway(
                          textStyle: TextStyle(
                              fontSize: 12,
                              color: Color(0xFF707B81),
                              height: 16/12
                          )
                      ),
                    ),
                    onTap: (){
                      Navigator.push(context, MaterialPageRoute(builder: (context) => ForgotPass()));
                    },
                  )
                ],
              ),
              SizedBox(height: 10,),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Checkbox(
                      value: check,
                      onChanged: (val){
                        check = val!;
                        setState(() {

                        });
                      }),
                  Text(
                    AppLocale.rememberPass.getString(context),
                    style: GoogleFonts.raleway(
                        textStyle: TextStyle(
                            fontSize: 12,
                            color: Color(0xFF707B81),
                            height: 16/12
                        )
                    ),
                  )
                ],
              ),
              SizedBox(height: 24,),
              Align(
                alignment: Alignment.center,
                child: SizedBox(
                  height: 50,
                  width: double.infinity,
                  child: FilledButton(
                    onPressed: ()async{
                      showLoading(context);
                      await useCase.pressButton(email.text,
                          password.text,
                              (_)async{
                        if (check){
                          await loadPassword(password.text);
                        }
                            hideLoading(context);
                            Navigator.push(context, MaterialPageRoute(builder: (context) => Home()));

                          },
                              (String error)async{
                            hideLoading(context);
                            showError(context, error);
                          }
                      );

                    },
                    child: Text(
                      AppLocale.signInButton.getString(context),
                      style: GoogleFonts.raleway(
                          textStyle: TextStyle(
                              fontSize: 14,
                              color: Colors.white,
                              fontWeight: FontWeight.w600,
                              height: 22/14
                          )
                      ),
                    ),
                    style: FilledButton.styleFrom(
                        backgroundColor: Color(0xFF48B2E7),
                        disabledBackgroundColor: Color(0xFF48B2E7),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(13)
                        )
                    ),
                  ),
                ),
              ),
              SizedBox(height: 24,),
              Align(
                alignment: Alignment.center,
                child: SizedBox(
                  height: 50,
                  width: double.infinity,
                  child: FilledButton(
                    onPressed: ()async{
                      showLoading(context);
                      await useCase.pressButtonGoogle(
                              (_){
                            hideLoading(context);
                            Navigator.push(context, MaterialPageRoute(builder: (context) => SignIn()));

                          },
                              (String error)async{
                            hideLoading(context);
                            showError(context, error);
                          }
                      );
                    },
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        SvgPicture.asset('assets/google.svg'),
                        SizedBox(width: 14,),
                        Text(
                          AppLocale.googleInButton.getString(context),
                          style: GoogleFonts.raleway(
                              textStyle: TextStyle(
                                  fontSize: 14,
                                  color: Color(0xFF2B2B2B),
                                  fontWeight: FontWeight.w600,
                                  height: 22/14
                              )
                          ),
                        ),
                      ],
                    ),
                    style: FilledButton.styleFrom(
                        backgroundColor: Color(0xFFF7F7F9),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(13)
                        )
                    ),
                  ),
                ),
              ),
              SizedBox(height: 135,),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    AppLocale.newUser.getString(context),
                    style: GoogleFonts.raleway(
                        textStyle: TextStyle(
                            fontSize: 16,
                            color: Color(0xFF6A6A6A),
                            fontWeight: FontWeight.w500,
                            height: 19/16
                        )
                    ),
                  ),
                  InkWell(
                    onTap: (){

                      Navigator.push(context, MaterialPageRoute(builder: (context) => SignUp()));
                    },
                    child:  Text(
                      AppLocale.createAc.getString(context),
                      style: GoogleFonts.raleway(
                          textStyle: TextStyle(
                              fontSize: 16,
                              color: Color(0xFF1A1D1E),
                              fontWeight: FontWeight.w500,
                              height: 19/16
                          )
                      ),
                    ),
                  )
                ],
              )


            ],
          ),
        ),
      ),
    );
  }
}
