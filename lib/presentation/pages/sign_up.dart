
import 'package:change_case/change_case.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localization/flutter_localization.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:new_sessions/domain/sign_up_use_case.dart';
import 'package:new_sessions/presentation/localization.dart';
import 'package:new_sessions/presentation/pages/watch_pdf.dart';
import 'package:new_sessions/presentation/pages/sign_in.dart';
import 'package:new_sessions/presentation/utils/dialogs.dart';
import 'package:new_sessions/presentation/widgets/custom_field.dart';
import 'package:pdfx/pdfx.dart';

class SignUp extends StatefulWidget {
  const SignUp({super.key});

  @override
  State<SignUp> createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  var name = TextEditingController();
  var email = TextEditingController();
  var password = TextEditingController();
  bool check = false;
  SignUpUseCase useCase = SignUpUseCase();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: Key('SignUp'),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 66,),
              Container(
                height: 44,
                  width: 44,
                  decoration: BoxDecoration(
                    shape: BoxShape.rectangle,
                    borderRadius: BorderRadius.circular(40),
                    color: Color(0xFFF7F7F9)
                  ),
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 19.25, vertical: 16.25),
                    child: SvgPicture.asset('assets/back.svg', width: 5.5, height: 11.5,),
                  )
              ),
              SizedBox(height: 11,),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    AppLocale.signUpTitle.getString(context),
                    textAlign: TextAlign.center,
                    style: GoogleFonts.raleway(
                        textStyle: TextStyle(
                            fontSize: 32,
                            color: Color(0xFF2B2B2B),
                            fontWeight: FontWeight.w700,
                            height: 38/32
                        )
                    ),
                  ),

                ],
              ),
              SizedBox(height: 8,),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    AppLocale.signUpText.getString(context),
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                        textStyle: TextStyle(
                            fontSize: 16,
                            color: Color(0xFF707B81),
                            height: 24/16
                        )
                    ),
                  )
                ],
              ),
              SizedBox(height: 30,),
              CustomField(label: AppLocale.name.getString(context), hint: 'xxxxxxxx', controller: name),
              SizedBox(height: 12,),
              CustomField(label: AppLocale.email.getString(context), hint: 'xyz@gmail.com', controller: email),
              SizedBox(height: 30,),
              CustomField(label: AppLocale.password.getString(context), hint: '••••••••', controller: password, enableObscure: true,),
              SizedBox(height: 10,),
              Row(
                children: [
                  InkWell(
                    child: Container(
                      width: 18,
                      height: 18,
                      decoration: BoxDecoration(
                          color: (!check)?Color(0xFFF7F7F9):Colors.greenAccent,
                          borderRadius: BorderRadius.circular(6)
                      ),
                      child: Padding(
                          padding: EdgeInsets.symmetric(horizontal: 6, vertical: 5.25),
                          child: SvgPicture.asset("assets/personal.svg")
                      ),
                    ),
                    onTap: (){
                      check = !check;
                      setState(() {

                      });
                    },
                  ),
                  SizedBox(width: 33,),
                  InkWell(
                    onTap: (){
                      showLoading(context);
                      final pdfPinchController = PdfControllerPinch(
                        document: PdfDocument.openAsset('assets/file.pdf'),
                      );
                      hideLoading(context);
                      Navigator.push(context, MaterialPageRoute(builder: (context) => WatchPdf(pdfController: pdfPinchController)));
                    },

                      child: Text(
                        textAlign: TextAlign.start,
                        AppLocale.pdf.getString(context),

                        style: GoogleFonts.raleway(
                            textStyle: TextStyle(
                                decoration: TextDecoration.underline,
                                fontSize: 16,
                                color: Color(0xFF6A6A6A),
                                height: 19/16,
                                fontWeight: FontWeight.w500
                            )
                        ),
                      ),

                  )
                ],
              ),
              SizedBox(height: 10,),
              Align(
                alignment: Alignment.center,
                child: SizedBox(
                  height: 50,
                  width: double.infinity,
                  child: FilledButton(
                    onPressed: (check)?()async{
                      showLoading(context);
                      await useCase.pressButton(name.text,
                          email.text,
                          password.text,
                              (_){
                        hideLoading(context);
                        Navigator.push(context, MaterialPageRoute(builder: (context) => SignIn()));

                              },
                          (String error)async{
                        hideLoading(context);
                        showError(context, error);
                          }
                      );
                    }:null,
                    child: Text(
                      AppLocale.signUpButton.getString(context),
                      style: GoogleFonts.raleway(
                          textStyle: TextStyle(
                              fontSize: 14,
                              color: Colors.white,
                              fontWeight: FontWeight.w600,
                              height: 22/14
                          )
                      ),
                    ),
                    style: FilledButton.styleFrom(
                      backgroundColor: Color(0xFF48B2E7),
                      disabledBackgroundColor: Color(0xFF48B2E7),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(13)
                      )
                    ),
                  ),
                ),
              ),
              SizedBox(height: 24,),
              Align(
                alignment: Alignment.center,
                child: SizedBox(
                  height: 50,
                  width: double.infinity,
                  child: FilledButton(
                    onPressed: ()async{
                      showLoading(context);
                      await useCase.pressButtonGoogle(
                              (_){
                            hideLoading(context);
                            Navigator.push(context, MaterialPageRoute(builder: (context) => SignIn()));

                          },
                              (String error)async{
                            hideLoading(context);
                            showError(context, error);
                          }
                      );
                    },
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        SvgPicture.asset('assets/google.svg'),
                        SizedBox(width: 14,),
                        Text(
                          AppLocale.googleUpButton.getString(context),
                          style: GoogleFonts.raleway(
                              textStyle: TextStyle(
                                  fontSize: 14,
                                  color: Color(0xFF2B2B2B),
                                  fontWeight: FontWeight.w600,
                                  height: 22/14
                              )
                          ),
                        ),
                      ],
                    ),
                    style: FilledButton.styleFrom(
                        backgroundColor: Color(0xFFF7F7F9),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(13)
                        )
                    ),
                  ),
                ),
              ),
              SizedBox(height: 37,),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    AppLocale.already.getString(context),
                    style: GoogleFonts.raleway(
                        textStyle: TextStyle(
                            fontSize: 16,
                            color: Color(0xFF6A6A6A),
                            fontWeight: FontWeight.w500,
                            height: 19/16
                        )
                    ),
                  ),
                  InkWell(
                    onTap: (){
                      Navigator.push(context, MaterialPageRoute(builder: (context) => SignIn()));
                    },
                    child:  Text(
                      AppLocale.logIn.getString(context),
                      style: GoogleFonts.raleway(
                          textStyle: TextStyle(
                              fontSize: 16,
                              color: Color(0xFF1A1D1E),
                              fontWeight: FontWeight.w500,
                              height: 19/16
                          )
                      ),
                    ),
                  )
                ],
              )


            ],
          ),
        ),
      ),
    );
  }
}
