import 'dart:async';

import 'package:change_case/change_case.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localization/flutter_localization.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:new_sessions/data/models/model_product.dart';
import 'package:new_sessions/domain/checkout_use_case.dart';
import 'package:new_sessions/domain/map_use_case.dart';
import 'package:new_sessions/presentation/localization.dart';
import 'package:new_sessions/presentation/pages/map.dart';
import 'package:new_sessions/presentation/utils/dialogs.dart';
import 'package:new_sessions/presentation/widgets/custom_field.dart';
import 'package:new_sessions/presentation/widgets/order_dialog.dart';
import 'package:pinput/pinput.dart';
import 'package:yandex_mapkit/yandex_mapkit.dart';

class Checkout extends StatefulWidget {
  Checkout({super.key, required this.allProducts, required this.summa, required this.delivery});
  List<ModelProduct> allProducts;
  double summa;
  double delivery;


  @override
  State<Checkout> createState() => _CheckoutState();
}

class _CheckoutState extends State<Checkout> {
  MapUseCase useCase = MapUseCase();
  late final YandexMapController _mapController;
  var _mapZoom = 0.0;
  Completer<YandexMapController> _completer = Completer();

  double? latitude;
  double? longitude;
  bool flag = false;
  CheckoutUseCase checkoutUseCase = CheckoutUseCase();
  String? email;
  String? phone;
  int? orderId;
  String? cardName;
  String? cardNumber;
  var emailController = TextEditingController();
  var phoneController = TextEditingController();
  var carNameController = TextEditingController();
  var cardNumberController = TextEditingController();

  String address = '';

  CameraPosition? _userLocation;


  @override
  void dispose() {
    _mapController.dispose();
    super.dispose();
  }
  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    WidgetsBinding.instance.addPostFrameCallback((timeStamp)async {
      await useCase.getCurrentLocation(
              (Position position){
            latitude = position.latitude;
            longitude = position.longitude;
            useCase.getAddress(
                Point(latitude: position.latitude, longitude: position.longitude),
                    (String ad){
                  address = ad;
                  setState(() {

                  });
                },
                    (String e)async{
                  showError(context, e);
                });
          },
              (String e)async{
            showError(context, e);
          });
      checkoutUseCase.emailandPhone(
              (response) {
                email = response[0];
                phone = response[1];
                cardName = response[2];
                cardNumber = response[3];
                emailController.text = email!;
                phoneController.text = phone!;
                carNameController.text = cardName!;
                cardNumberController.text = cardNumber!;
                flag = true;
                setState(() {

                });
                },
              (String e)async{
            showError(context, e);
          });
    });
  }

  Future<void> editEmailOrPhone(TextEditingController controller, String title)async{
    await showDialog(context: context,
        builder: (_) => AlertDialog(
          content: CustomField(label: '', hint: title, controller: controller),
          title: Text('Get your $title'),
          actions: [
            TextButton(onPressed: (){
              if (title == 'email'){
                email = controller.text;
              }
              else{
                phone = controller.text;
              }
              setState(() {

              });
              Navigator.of(context).pop();}, child: Text('Ok'))
          ],
        ));
  }

  Future<void> editCard()async{
    await showDialog(context: context,
        builder: (_) => AlertDialog(
          content: Column(
            children: [
              CustomField(label: 'Card name', hint: '', controller: carNameController),
              SizedBox(height: 8,),
              CustomField(label: 'Card number', hint: '', controller: cardNumberController)
            ],
          ),
          title: Text('Get your card'),
          actions: [
            TextButton(onPressed: (cardNumberController.length ==16)?(){
              cardNumber = cardNumberController.text;
              cardName = carNameController.text;
              setState(() {

              });
              Navigator.of(context).pop();}:null, child: Text('Ok'))
          ],
        ));
  }




  @override
  Widget build(BuildContext context) {
    return (flag)?Scaffold(
      backgroundColor: Color(0xFFF7F7F9),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
              padding: EdgeInsets.only(top: 48, left: 20, right: 20),
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Align(
                          alignment: Alignment.topLeft,
                          child: InkWell(
                            child: Container(
                                height: 44,
                                width: 44,
                                decoration: BoxDecoration(
                                    shape: BoxShape.rectangle,
                                    borderRadius: BorderRadius.circular(40),
                                    color: Colors.white
                                ),
                                child: Padding(
                                  padding: EdgeInsets.symmetric(horizontal: 19.25, vertical: 16.25),
                                  child: SvgPicture.asset('assets/back.svg', width: 5.5, height: 11.5,),
                                )
                            ),
                            onTap: (){
                              Navigator.of(context).pop();
                            },
                          )
                      ),
                      Center(
                          child:
                          Align(
                            alignment: Alignment.topCenter,
                            child: Padding(
                              padding: EdgeInsets.only(top: 6),
                              child: Text(
                                  'My Cart'.toCapitalCase(),
                                  style: GoogleFonts.raleway(
                                      textStyle: TextStyle(
                                          fontWeight: FontWeight.w600,
                                          color: Color(0xFF2B2B2B),
                                          height: 20/16,
                                          fontSize: 16)
                                  )
                              ),
                            ),
                          )

                      ),
                      Container(
                        height: 44,
                        width: 44,

                      ),

                    ],
                  ),
                   SizedBox(
                     height: 46,
                   ),

                  ])
          ),
          Expanded(child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 14),
            child: ListView.builder(
              itemCount: 1,
                shrinkWrap: true,
                itemBuilder: (_, index){
                return Container(
                  height: 427,
                  width: double.infinity,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(16),
                      border: Border.all(color: Colors.transparent)
                  ),
                  child: Padding(
                    padding: EdgeInsets.symmetric(vertical: 16, horizontal: 20),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                            AppLocale.contactInfo.getString(context),
                            style: GoogleFonts.raleway(
                                textStyle: TextStyle(
                                    fontWeight: FontWeight.w500,
                                    color: Color(0xFF2B2B2B),
                                    height: 20/14,
                                    fontSize: 14)
                            )
                        ),
                        SizedBox(height: 16,),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(
                              children: [
                                Container(
                                  height: 40,
                                  width: 40,
                                  decoration: BoxDecoration(
                                      color: Color(0xFFF7F7F9),
                                      border: Border.all(color: Colors.transparent),
                                      borderRadius: BorderRadius.circular(12)
                                  ),
                                  child: Padding(
                                    padding: EdgeInsets.all(10),
                                    child: SvgPicture.asset('assets/email_contact.svg'),
                                  ),
                                ),
                                SizedBox(width: 12,),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                        (email == '')?'*****************@*******.***':email!,
                                        style: GoogleFonts.raleway(
                                            textStyle: TextStyle(
                                                color: Color(0xFF2B2B2B),
                                                height: 20/14,
                                                fontSize: 14)
                                        )
                                    ),
                                    SizedBox(height: 4,),
                                    Text(
                                        'Email'.toCapitalCase(),
                                        style: GoogleFonts.raleway(
                                            textStyle: TextStyle(
                                                color: Color(0xFF6A6A6A),
                                                height: 16/12,
                                                fontSize: 12)
                                        )
                                    ),
                                  ],
                                )
                              ],

                            ),
                            InkWell(
                              child: SvgPicture.asset('assets/edit.svg'),
                              onTap: (){
                                editEmailOrPhone(emailController, 'email');
                                setState(() {

                                });
                              },
                            )
                          ],
                        ),
                        SizedBox(height: 16,),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(
                              children: [
                                Container(
                                  height: 40,
                                  width: 40,
                                  decoration: BoxDecoration(
                                      color: Color(0xFFF7F7F9),
                                      border: Border.all(color: Colors.transparent),
                                      borderRadius: BorderRadius.circular(12)
                                  ),
                                  child: Padding(
                                    padding: EdgeInsets.all(10),
                                    child: SvgPicture.asset('assets/phone.svg'),
                                  ),
                                ),
                                SizedBox(width: 12,),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                        (phone == '')?'+***-***-***-****':phone!,
                                        style: GoogleFonts.raleway(
                                            textStyle: TextStyle(
                                                color: Color(0xFF2B2B2B),
                                                height: 20/14,
                                                fontSize: 14)
                                        )
                                    ),
                                    SizedBox(height: 4,),
                                    Text(
                                        AppLocale.phone.getString(context),
                                        style: GoogleFonts.raleway(
                                            textStyle: TextStyle(
                                                color: Color(0xFF6A6A6A),
                                                height: 16/12,
                                                fontSize: 12)
                                        )
                                    ),
                                  ],
                                )
                              ],

                            ),
                            InkWell(
                              child: SvgPicture.asset('assets/edit.svg'),
                              onTap: (){
                                editEmailOrPhone(phoneController, 'phone');
                                setState(() {

                                });
                              },
                            )
                          ],
                        ),
                        SizedBox(height: 12,),
                        Text(
                            AppLocale.address.getString(context),
                            style: GoogleFonts.raleway(
                                textStyle: TextStyle(
                                    color: Color(0xFF2B2B2B),
                                    height: 20/14,
                                    fontWeight: FontWeight.w500,
                                    fontSize: 14)
                            )
                        ),
                        SizedBox(height: 12,),

                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                                address,
                                style: GoogleFonts.raleway(
                                    textStyle: TextStyle(
                                        color: Color(0xFF6A6A6A),
                                        height: 16/12,
                                        fontSize: 12)
                                )
                            ),

                            Padding(padding: EdgeInsets.only(right: 12),
                                child: SvgPicture.asset('assets/down.svg'))

                          ],
                        ),
                        SizedBox(height: 16,),
                        Padding(padding: EdgeInsets.only(right: 12),
                        child: Container(
                          height: 101,
                          width: double.infinity,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(16),
                              border: Border.all(color: Colors.transparent)
                              ),
                          child: YandexMap(
                            onMapCreated: (controller) async {
                              _mapController = controller;

                              await _mapController.toggleUserLayer(visible: true);
                              _completer.complete(controller);
                            },
                            onUserLocationAdded: (view) async {
                              _userLocation = await _mapController.getUserCameraPosition();
                              if (_userLocation != null) {
                                await _mapController.moveCamera(
                                  CameraUpdate.newCameraPosition(
                                    _userLocation!.copyWith(zoom: 10),
                                  ),
                                  animation: const MapAnimation(
                                    type: MapAnimationType.linear,
                                    duration: 0.3,
                                  ),
                                );
                              }
                              
                              return view.copyWith(
                                 pin: PlacemarkMapObject(
                                     mapId: MapObjectId('My location'),
                                     point: Point(latitude: latitude!, longitude: longitude!),
                                     opacity: 1,
                                     onTap: (_, a){
                                       Navigator.push(context, MaterialPageRoute(builder: (context) => MapPage(latitude: latitude!, longitude: longitude!)));
                                       },
                                     icon: PlacemarkIcon.single(
                                         PlacemarkIconStyle(
                                           scale: 1,
                                           image: BitmapDescriptor.fromAssetImage(
                                               "assets/blue.png"
                                           ),

                                         )
                                     ))
                              );
                            },
                          ),
                        )

                        //   child: Container(
                        //     height: 101,
                        //     width: double.infinity,
                        //     decoration: BoxDecoration(
                        //         borderRadius: BorderRadius.circular(16),
                        //         border: Border.all(color: Colors.transparent)
                        //     ),
                        //     child: Column(
                        //       crossAxisAlignment: CrossAxisAlignment.center,
                        //       children: [
                        //         Text(
                        //             AppLocale.watchMap.getString(context),
                        //             style: GoogleFonts.raleway(
                        //                 textStyle: TextStyle(
                        //                     color: Colors.white,
                        //                     height: 21/20,
                        //                     fontWeight: FontWeight.w700,
                        //                     fontSize: 20)
                        //             )
                        //         ),
                        //         SizedBox(height: 6,),
                        //         Container(
                        //           height: 36,
                        //           width: 36,
                        //           decoration: BoxDecoration(
                        //               color: Color(0xFF0D6EFD),
                        //               borderRadius: BorderRadius.circular(30),
                        //               border: Border.all(color: Colors.transparent)
                        //           ),
                        //           child: Padding(
                        //               padding: EdgeInsets.symmetric(vertical: 9.67, horizontal: 10.5),
                        //               child: InkWell(
                        //                 child: SvgPicture.asset('assets/combo shape.svg'),
                        //                 onTap: (){
                        //                   Navigator.push(context, MaterialPageRoute(builder: (context) => MapPage(latitude: latitude!, longitude: longitude!)));
                        //                 },
                        //               )
                        //           ),
                        //   )
                        //   ],
                        //   ),
                        //
                        // ),
                        ),
                        SizedBox(height: 12,),
                        Text(
                            AppLocale.paymentMethod.getString(context),
                            style: GoogleFonts.raleway(
                                textStyle: TextStyle(
                                    fontWeight: FontWeight.w500,
                                    color: Color(0xFF2B2B2B),
                                    height: 20/14,
                                    fontSize: 14)
                            )
                        ),
                        SizedBox(height: 12,),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(
                              children: [
                                Container(
                                  height: 40,
                                  width: 40,
                                  decoration: BoxDecoration(
                                      color: Color(0xFFF7F7F9),
                                      border: Border.all(color: Colors.transparent),
                                      borderRadius: BorderRadius.circular(12)
                                  ),
                                  child: Padding(
                                    padding: EdgeInsets.only(top: 10, left: 4, right: 4, bottom: 8),
                                    child: Image.asset('assets/card.png'),
                                  ),
                                ),
                                SizedBox(width: 12,),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                        (cardName == '')?'Добавить':cardName!,
                                        style: GoogleFonts.raleway(
                                            textStyle: TextStyle(
                                                color: Color(0xFF2B2B2B),
                                                height: 20/14,
                                                fontWeight: FontWeight.w500,
                                                fontSize: 14)
                                        )
                                    ),
                                    SizedBox(height: 4,),
                                    Text(
                                        (cardNumber == '')?'**** ****':'**** **** ${cardNumber?.substring(8, 12)} ${cardNumber?.substring(12, 16)}',
                                        style: GoogleFonts.raleway(
                                            textStyle: TextStyle(
                                                color: Color(0xFF6A6A6A),
                                                height: 16/12,
                                                fontSize: 12)
                                        )
                                    ),
                                  ],
                                )
                              ],

                            ),

                            Padding(padding: EdgeInsets.only(right: 12),
                              child: InkWell(
                                child: SvgPicture.asset('assets/down.svg'),
                                onTap: (){
                                  editCard();
                                  setState(() {

                                  });
                                },
                              ),),
                          ],
                        ),

                      ],

                    ),
                  ),
                );
                }),
          )),



          Align(
            alignment: Alignment.bottomLeft,
            child: Container(
              height: 258,
              width: double.infinity,
              decoration: BoxDecoration(
                  color: Colors.white
              ),
              child: Padding(
                padding: EdgeInsets.only(left: 20, right: 20, top: 37, bottom: 28),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          AppLocale.summa.getString(context),
                          style: GoogleFonts.raleway(
                              textStyle: TextStyle(
                                  color: Color(0xFF707B81),
                                  fontSize: 16,
                                  height: 20/16,
                                  fontWeight: FontWeight.w500
                              )
                          ),
                        ),
                        Text(
                          '₽${widget.summa}',
                          style: GoogleFonts.poppins(
                              textStyle: TextStyle(
                                color: Color(0xFF2B2B2B),
                                fontSize: 16,
                                height: 24/16,

                              )
                          ),
                        ),

                      ],
                    ),
                    SizedBox(height: 8,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          AppLocale.delivery.getString(context),
                          style: GoogleFonts.raleway(
                              textStyle: TextStyle(
                                  color: Color(0xFF707B81),
                                  fontSize: 16,
                                  height: 20/16,
                                  fontWeight: FontWeight.w500
                              )
                          ),
                        ),
                        Text(
                          '₽${widget.delivery}',
                          style: GoogleFonts.poppins(
                              textStyle: TextStyle(
                                color: Color(0xFF2B2B2B),
                                fontSize: 16,
                                height: 24/16,

                              )
                          ),
                        ),

                      ],
                    ),
                    SizedBox(height: 16,),
                    Divider(height: 1, color: Color(0xFF707B81)),
                    SizedBox(height: 16,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          AppLocale.totalCost.getString(context),
                          style: GoogleFonts.raleway(
                              textStyle: TextStyle(
                                  color: Color(0xFF2B2B2B),
                                  fontSize: 16,
                                  height: 20/16,
                                  fontWeight: FontWeight.w500
                              )
                          ),
                        ),
                        Text(
                          '₽${widget.delivery + widget.summa}',
                          style: GoogleFonts.poppins(
                              textStyle: TextStyle(
                                color: Color(0xFF48B2E7),
                                fontSize: 16,
                                height: 24/16,

                              )
                          ),
                        ),

                      ],
                    ),

                    SizedBox(height: 30,),
                    Align(
                      alignment: Alignment.center,
                      child: SizedBox(
                        height: 50,
                        width: double.infinity,
                        child: FilledButton(
                          onPressed: ()async{
                            showLoading(context);
                            checkoutUseCase.loadParametrsUser(
                                email!,
                                phone!, cardName!, cardNumber!,
                                    (_)async{
                                  await checkoutUseCase.makeWholeOrder(
                                      email!,
                                      phone!,
                                      cardName!,
                                      cardNumber!,
                                      address,
                                          (id) async {
                                        orderId = id;
                                        for (var val in widget.allProducts){
                                          await checkoutUseCase.makeWholeOrderItems(
                                              val.title,
                                              val.cost * val.count,
                                              val.count,
                                              orderId!,
                                                  val.id,
                                                  (_) {},
                                                  (String error)async{
                                                hideLoading(context);
                                                await showError(context, error);
                                              }
                                          );
                                        }

                                        checkoutUseCase.clearWholeBasket(
                                                (p0){
                                                  hideLoading(context);
                                                  showOrderDialog(context);
                                                },
                                                (String error)async{
                                              hideLoading(context);
                                              await showError(context, error);
                                            }
                                        );

                                          },
                                          (String error)async{
                                        hideLoading(context);
                                        await showError(context, error);
                                      }
                                  );
                                    },
                                    (String error)async{
                                  hideLoading(context);
                                  await showError(context, error);
                                    }
                            );

                          },
                          child: Text(
                            AppLocale.checkout2.getString(context).toString(),
                            style: GoogleFonts.raleway(
                                textStyle: TextStyle(
                                    fontSize: 14,
                                    color: Colors.white,
                                    fontWeight: FontWeight.w500,
                                    height: 22/14
                                )
                            ),
                          ),
                          style: FilledButton.styleFrom(
                              backgroundColor: Color(0xFF48B2E7),
                              disabledBackgroundColor: Color(0xFF48B2E7),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(12)
                              )
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          )

        ],
      ),
       ):Center(child: CircularProgressIndicator(),);
  }
}
