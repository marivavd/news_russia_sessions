import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localization/flutter_localization.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:new_sessions/data/models/model_category.dart';
import 'package:new_sessions/data/models/model_product.dart';
import 'package:new_sessions/domain/basket_use_case.dart';
import 'package:new_sessions/domain/favorite_use_case.dart';
import 'package:new_sessions/domain/main_page_use_case.dart';
import 'package:new_sessions/presentation/localization.dart';
import 'package:new_sessions/presentation/pages/cart.dart';
import 'package:new_sessions/presentation/pages/listning.dart';
import 'package:new_sessions/presentation/pages/menu.dart';
import 'package:new_sessions/presentation/pages/orders.dart';
import 'package:new_sessions/presentation/pages/popular.dart';
import 'package:new_sessions/presentation/pages/search.dart';
import 'package:new_sessions/presentation/utils/dialogs.dart';
import 'package:new_sessions/presentation/widgets/card_product.dart';

class MainPage extends StatefulWidget {
  const MainPage({super.key});

  @override
  State<MainPage> createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  MainPageUseCase useCase = MainPageUseCase();
  List<ModelCategory> categories = [];
  bool flag = false;
  ModelProduct? product;
  List<ModelProduct> products = [];
  int selectedCategoryIndex = 0;
  BasketUseCase basketUseCase = BasketUseCase();
  FavouriteUseCase favouriteUseCase = FavouriteUseCase();
  List<bool> isFavourites = [];
  List<bool> isBasket = [];
  String urlAdvert = '';


  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async{

      await useCase.makeModelsCategories((response){

        categories = response;

        setState(() {

        });
        },
          (String e)async{

        showError(context, e);
          });
      await useCase.getAdvertisimet(
              (response) {
            urlAdvert = response;

          },
              (String e) async{
            await showError(context, e);
          }
      );
      await useCase.makeModelsProductsBest(
              (response) {
            products = response;

          },
              (String e) async{
            await showError(context, e);
          });
      for (var val in products) {
        await basketUseCase.isInBasket(
            val.id,
                (response) {
              isBasket.add(response);
            },
                (String error) async {
              await showError(context, error);
            }
        );
        await favouriteUseCase.isInFavourite(
            val.id,
                (response) {
              isFavourites.add(response);
            },
                (String error) async {
              await showError(context, error);
            }
        );
      }
      flag = true;
      setState(() {

      });
    }
    );
}


  @override
  Widget build(BuildContext context) {

    return (flag)?Scaffold(
      backgroundColor: Color(0xFFF7F7F9),
      body: Padding(
          padding: EdgeInsets.only(top: 48, left: 20, right: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  InkWell(
                    child: SvgPicture.asset('assets/Hamburger.svg'),
                    onTap: (){
                      Navigator.push(context, MaterialPageRoute(builder: (context) => Menu()));
                    },
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Transform.translate(
                          offset: const Offset(3, 0),
                          child: SvgPicture.asset("assets/Highlight_05.svg")
                      ),
                      Text(AppLocale.main.getString(context),
                          style: GoogleFonts.raleway(
                            textStyle: TextStyle(
                                fontWeight: FontWeight.w700,
                                color: Color(0xFF2B2B2B),
                                height: 38/32,
                                fontSize: 32)
                          ))
                    ],
                  ),
                  Container(
                    height: 44,
                    width: 44,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(40),
                      color: Colors.white
                    ),
                    child:
                    // Column(
                    //   children: [
                    //     SizedBox(height: 3,),
                    //     Row(
                    //       mainAxisAlignment: MainAxisAlignment.end,
                    //       children: [
                    //         SvgPicture.asset('assets/ellipse.svg')
                    //       ],
                    //     ),
                    //    Padding(padding: EdgeInsets.only(bottom: 9),
                    //    child: SvgPicture.asset('assets/bag.svg'))
                    //   ],
                    // ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        SizedBox(width: 10,),
                        Padding(padding: EdgeInsets.symmetric(vertical: 10),
                            child: InkWell(
                              child: SvgPicture.asset('assets/bag.svg'),
                              onTap: (){
                                Navigator.push(context, MaterialPageRoute(builder: (context) => Cart()));
                              },
                            )),


                        Padding(padding: EdgeInsets.only(bottom: 24),child: SvgPicture.asset('assets/ellipse.svg'),)


                      ],
                    ),
                  )

                ],
              ),
              SizedBox(height: 19,),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Expanded(child: InkWell(
                    onTap: (){
                      Navigator.push(context, MaterialPageRoute(builder: (context) => Search()));
                    },
                    child: Container(
                        height: 52,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(14),
                            border: Border.all(color: Colors.transparent)

                        ),
                        child: Padding(
                          padding: EdgeInsets.only(left: 26, top: 14, bottom: 14),
                          child: Row(
                            children: [
                              SvgPicture.asset('assets/search.svg'),
                              SizedBox(width: 12,),
                              Text(
                                  AppLocale.search.getString(context),
                                  style: GoogleFonts.poppins(
                                      textStyle: TextStyle(
                                          color: Color(0xFF6A6A6A),
                                          height: 20/12,
                                          fontSize: 12)
                                  )

                              )

                            ],
                          ),
                        )
                    ),
                  )),
                  SizedBox(width: 14,),
                  Container(
                    width: 52,
                    height: 52,
                    decoration: BoxDecoration(
                      color: Color(0xFF48B2E7),
                      border: Border.all(color: Colors.transparent),
                      borderRadius: BorderRadius.circular(40)
                    ),
                    child: Padding(
                      padding: EdgeInsets.symmetric(vertical: 14, horizontal: 14),
                      child: InkWell(
                          child: SvgPicture.asset('assets/sliders.svg'),
                        onTap: (){
                          Navigator.push(context, MaterialPageRoute(builder: (context) => Orders()));
                        },),
                    ),
                  )
                ],
              ),
              SizedBox(height: 24,),
              Text(
                AppLocale.category.getString(context),
                  style: GoogleFonts.raleway(
                      textStyle: TextStyle(
                          fontWeight: FontWeight.w600,
                          color: Color(0xFF2B2B2B),
                          height: 19/16,
                          fontSize: 16)
                  )
              ),
              SizedBox(height: 16,),
              SizedBox(height: 40,
                  child: ListView.separated(
                    scrollDirection: Axis.horizontal,
                    separatorBuilder: (_, __) => const SizedBox(width: 16),
                    itemCount: categories.length + 1,
                    itemBuilder: (_, index){
                      var category;
                      if (index == 0){
                        category = ModelCategory(id: '', title: AppLocale.categoryAll.getString(context));
                      }
                      else{
                        category = categories[index - 1];
                      }
                      return Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(8),
                            color: Colors.white,
                          ),
                          padding: const EdgeInsets.symmetric(vertical: 11, horizontal: 26),
                          constraints: const BoxConstraints(minWidth: 108),
                          child: GestureDetector(
                            onTap: (){
                              Navigator.push(context, MaterialPageRoute(builder: (context) => Listning(category: category!, categories: [ModelCategory(id: '', title: AppLocale.categoryAll.getString(context))]+categories, index: index,)));
                            },
                            child: Text(
                                category!.title,
                                textAlign: TextAlign.center,
                                style: TextStyle(color: Color(0xFF2B2B2B))),
                          )
                      );
                    },

                  )),
              const SizedBox(height: 24),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                   Text(
                     AppLocale.popular.getString(context),
                     style: GoogleFonts.raleway(
                         textStyle: const TextStyle(
                           fontSize: 16,
                           fontWeight: FontWeight.w500,
                           color: Color(0xFF2B2B2B),
                           height: 24/16,
                         )
                     ),
                   ),
                  InkWell(
                    onTap: (){
                      Navigator.push(context, MaterialPageRoute(builder: (context) => Popular(products: products,)));
                    },
                    child: Text(
                      AppLocale.seeAll.getString(context),
                      style: GoogleFonts.poppins(
                          textStyle: const TextStyle(
                            fontSize: 12,
                            color: Color(0xFF48B2E7),
                            height: 16/12,
                          )
                      ),
                    ),
                  )
                ],
              ),
              SizedBox(height: 16,),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CardItem(product: products[0],
                    isBasket: isBasket[0],
                    isFavourite: isFavourites[0],
                    ontapFavourite: ()async{
                      showLoading(context);
                      if (isFavourites[0]){
                        await favouriteUseCase.delFavourite(
                            products[0].id,
                                (_){
                              isFavourites[0] = false;
                              hideLoading(context);
                              setState(() {

                              });
                            },
                                (String e)async{
                              hideLoading(context);
                              showError(context, e);
                            }
                        );
                      }
                      else{
                        await favouriteUseCase.putInFavourite(
                            products[0].id,
                                (_){
                              isFavourites[0] = true;
                              hideLoading(context);
                              setState(() {

                              });
                            },
                                (String e)async{
                              hideLoading(context);
                              showError(context, e);
                            }
                        );
                      }
                    },
                    onTapBasket: ()async{
                      showLoading(context);
                      if (isBasket[0]){
                        await basketUseCase.updateBasket(
                            products[0].id,
                                (_){
                              isBasket[0] = true;
                              hideLoading(context);
                              setState(() {

                              });
                            },
                                (String e)async{
                              hideLoading(context);
                              showError(context, e);
                            }
                        );
                      }
                      else{
                        await basketUseCase.putInBasket(
                            products[0].id,
                                (_){
                              isBasket[0] = true;
                              hideLoading(context);
                              setState(() {

                              });
                            },
                                (String e)async{
                              hideLoading(context);
                              showError(context, e);
                            }
                        );
                      }
                    },
                  ),
                  SizedBox(width: MediaQuery.of(context).size.width - 40 - 320,),
                  CardItem(product: products[1],
                    isBasket: isBasket[1],
                    isFavourite: isFavourites[1],
                    ontapFavourite: ()async{
                      showLoading(context);
                      if (isFavourites[1]){
                        await favouriteUseCase.delFavourite(
                            products[1].id,
                                (_){
                              isFavourites[1] = false;
                              hideLoading(context);
                              setState(() {

                              });
                            },
                                (String e)async{
                              hideLoading(context);
                              showError(context, e);
                            }
                        );
                      }
                      else{
                        await favouriteUseCase.putInFavourite(
                            products[1].id,
                                (_){
                              isFavourites[1] = true;
                              hideLoading(context);
                              setState(() {

                              });
                            },
                                (String e)async{
                              hideLoading(context);
                              showError(context, e);
                            }
                        );
                      }
                    },
                    onTapBasket: ()async{
                      showLoading(context);
                      if (isBasket[1]){
                        await basketUseCase.updateBasket(
                            products[1].id,
                                (_){
                              isBasket[1] = true;
                              hideLoading(context);
                              setState(() {

                              });
                            },
                                (String e)async{
                              hideLoading(context);
                              showError(context, e);
                            }
                        );
                      }
                      else{
                        await basketUseCase.putInBasket(
                            products[1].id,
                                (_){
                              isBasket[1] = true;
                              hideLoading(context);
                              setState(() {

                              });
                            },
                                (String e)async{
                              hideLoading(context);
                              showError(context, e);
                            }
                        );
                      }
                    },)
                ],
              ),
              SizedBox(height: 40.5,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    AppLocale.sales.getString(context),
                    style: GoogleFonts.raleway(
                        textStyle: const TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w500,
                          color: Color(0xFF2B2B2B),
                          height: 24/16,
                        )
                    ),
                  ),
                  InkWell(
                    onTap: (){},
                    child: Text(
                      AppLocale.seeAll.getString(context),
                      style: GoogleFonts.poppins(
                          textStyle: const TextStyle(
                            fontSize: 12,
                            color: Color(0xFF48B2E7),
                            height: 16/12,
                          )
                      ),
                    ),
                  )
                ],
              ),
              SizedBox(height: 21,),
              Container(

                width: double.infinity,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(16),
                  border: Border.all(color: Colors.transparent)
                ),
                child: CachedNetworkImage(
                  imageUrl: urlAdvert,
                ),
              )




            ],
          ),
      )

    ):Center(child: CircularProgressIndicator(),);
  }
}
