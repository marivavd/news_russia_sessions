import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localization/flutter_localization.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:new_sessions/data/models/model_product.dart';
import 'package:new_sessions/domain/basket_use_case.dart';
import 'package:new_sessions/domain/details_use_case.dart';
import 'package:new_sessions/domain/favorite_use_case.dart';
import 'package:new_sessions/presentation/pages/cart.dart';
import 'package:new_sessions/presentation/utils/dialogs.dart';

import '../localization.dart';

class Details extends StatefulWidget {
  Details({super.key, required this.product});
  ModelProduct product;

  @override
  State<Details> createState() => _DetailsState();
}

class _DetailsState extends State<Details> {
  CarouselController buttonCarouselController = CarouselController();
  String category = '';
  DetailsUseCase useCase = DetailsUseCase();
  BasketUseCase basketUseCase = BasketUseCase();
  FavouriteUseCase favouriteUseCase = FavouriteUseCase();
  bool flag = false;
  bool isFavourite = false;
  bool isBasked = false;


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async{
      await useCase.makeCategoryName(
          widget.product.categoryId,
          (response){
            category = response;
          },
          (String error)async{
            await showError(context, error);
          }
      );
      await basketUseCase.isInBasket(
          widget.product.id,
              (response){
            isBasked = response;
          },
              (String error)async{
            await showError(context, error);
          }
      );
      await favouriteUseCase.isInFavourite(
          widget.product.id,
              (response){
            isFavourite = response;
            flag = true;
            setState(() {

            });
          },
              (String error)async{
            await showError(context, error);
          }
      );
    });
  }
  @override
  Widget build(BuildContext context) {
    return (flag)?Scaffold(
      backgroundColor: Color(0xFFF7F7F9),
      body: SingleChildScrollView(
        child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 48,),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                InkWell(
                  child: Container(
                      height: 44,
                      width: 44,
                      decoration: BoxDecoration(
                          shape: BoxShape.rectangle,
                          borderRadius: BorderRadius.circular(40),
                          color: Colors.white
                      ),
                      child: Padding(
                        padding: EdgeInsets.symmetric(horizontal: 19.25, vertical: 16.25),
                        child: SvgPicture.asset('assets/back.svg', width: 5.5, height: 11.5,),
                      )
                  ),
                  onTap: (){
                    Navigator.of(context).pop();
                  },
                ),
                Padding(
                  padding: EdgeInsets.only(top: 12),
                  child: Text(
                      'Sneaker Shop',
                      style: GoogleFonts.raleway(
                          textStyle: TextStyle(
                              fontWeight: FontWeight.w600,
                              color: Color(0xFF2B2B2B),
                              height: 20/16,
                              fontSize: 16)
                      )
                  ),
                ),
                Container(
                  height: 44,
                  width: 44,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(40),
                      color: Colors.white
                  ),
                  child:
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      SizedBox(width: 10,),
                      Padding(padding: EdgeInsets.symmetric(vertical: 10),
                          child: InkWell(
                          onTap: (){
                            Navigator.push(context, MaterialPageRoute(builder: (context) => Cart()));
                            },
                            child: SvgPicture.asset('assets/bag.svg'),

                      )
                      ),


                      Padding(padding: EdgeInsets.only(bottom: 24),child: SvgPicture.asset('assets/ellipse.svg'),)


                    ],
                  ),
                )

              ],
            ),
            SizedBox(height: 26,),
        Text(
        widget.product.title,
        style: GoogleFonts.raleway(
    textStyle: const TextStyle(
    fontSize: 26,
    fontWeight: FontWeight.w700,
    color: Color(0xFF2B2B2B),
    height: 62/52,
    ))
        ),
        SizedBox(height: 8,),
            Text(
                category,
                style: GoogleFonts.raleway(
                    textStyle: const TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w500,
                      color: Color(0xFF6A6A6A),
                      height: 19/16,
                    ))
            ),
        SizedBox(height: 8,),
        Text(
          '₽${widget.product.cost.toString()}',
            style: GoogleFonts.raleway(
                textStyle: const TextStyle(
                  fontSize: 24,
                  color: Color(0xFF2B2B2B),
                  height: 35/24,
                ))

        ),
        CarouselSlider.builder(
          carouselController: buttonCarouselController,

          options: CarouselOptions(
            height: 255,
            autoPlay: false,
            onPageChanged: (index, f)async{

            },

          ),
          itemCount: widget.product.covers.length,
          itemBuilder: (context, itemIndex, realIndex) {
            return Padding(
                padding: const EdgeInsets.symmetric(horizontal: 17),
                child: Container(
                  height: 143.2,
                  child: CachedNetworkImage(
                    width: double.maxFinite,
                    fit: BoxFit.cover,
                    imageUrl: widget.product.covers[itemIndex],),
                )
            );
          }),
            SizedBox(height: 37,),
            SizedBox(
              height: 56,
              child: ListView.separated(
                scrollDirection: Axis.horizontal,
                separatorBuilder: (_, index){
                  return SizedBox(width: 14,);
                },
                itemCount: widget.product.covers.length,
                  itemBuilder: (_, index){
                  return Container(
                    height: 56,
                    width: 56,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(16),
                      border: Border.all(color: Colors.transparent)
                    ),
                    child: CachedNetworkImage(
                      imageUrl: widget.product.covers[index],
                      height: 21.08,
                    ),
                  );
                  },
                  ),
            ),
            SizedBox(height: 33,),
            Text(
              widget.product.description,
              maxLines: 3,
              style: GoogleFonts.poppins(
                  textStyle: const TextStyle(
                    fontSize: 14,
                    color: Color(0xFF6A6A6A),
                    height: 72/42,
                  )),
            ),
            SizedBox(height: 9,),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                InkWell(
                  child: Text(
                    AppLocale.readMore.getString(context),
                    style: GoogleFonts.poppins(
                        textStyle: const TextStyle(
                          fontSize: 14,
                          color: Color(0xFF0D6EFD),
                          height: 21/14,
                        )),
                  ),
                  onTap: (){},
                )
              ],
            ),
            SizedBox(height: 60,),
            Row(
              children: [
                SizedBox(width: 28,),
                InkWell(
                  onTap: ()async{
                    showLoading(context);
                    if (isFavourite){
                      await favouriteUseCase.delFavourite(
                          widget.product.id,
                              (_){
                            isFavourite = false;
                            hideLoading(context);
                            setState(() {

                            });
                          },
                              (String e)async{
                            hideLoading(context);
                            showError(context, e);
                          }
                      );
                    }
                    else{
                      await favouriteUseCase.putInFavourite(
                          widget.product.id,
                              (_){
                            isFavourite = true;
                            hideLoading(context);
                            setState(() {

                            });
                          },
                              (String e)async{
                            hideLoading(context);
                            showError(context, e);
                          }
                      );
                    }
                  },
                  child: Container(
                      height: 52,
                      width: 52,
                      decoration: BoxDecoration(
                          shape: BoxShape.rectangle,
                          borderRadius: BorderRadius.circular(40),
                          color: Color(0xFFD9D9D9)
                      ),
                      child: Padding(
                        padding: EdgeInsets.symmetric(horizontal: 14, vertical: 14),
                        child: SvgPicture.asset((isFavourite)?'assets/heart.svg':'assets/heart_no.svg', width: 24, height: 24,),
                      )
                  ),
                ),
                SizedBox(width: 40,),
                Expanded(child: FilledButton(
                  onPressed: ()async{
                    showLoading(context);
                    if (isBasked){
                      await basketUseCase.updateBasket(
                          widget.product.id,
                              (_){
                            isBasked = true;
                            hideLoading(context);
                            setState(() {

                            });
                          },
                              (String e)async{
                            hideLoading(context);
                            showError(context, e);
                          }
                      );
                    }
                    else{
                      await basketUseCase.putInBasket(
                          widget.product.id,
                              (_){
                            isBasked = true;
                            hideLoading(context);
                            setState(() {

                            });
                          },
                              (String e)async{
                            hideLoading(context);
                            showError(context, e);
                          }
                      );
                    }
                  },
                  child: Padding(
                    padding: EdgeInsets.symmetric(vertical: 13),
                    child: Row(
                      children: [
                        SizedBox(width: 46,),
                        SvgPicture.asset('assets/bag.svg', color: Colors.white, height: 22,),
                        SizedBox(width: 16,),
                        Text(
                            (isBasked)?AppLocale.alreadyBasket.getString(context):AppLocale.toBasket.getString(context),
                          style: GoogleFonts.raleway(
                              textStyle: TextStyle(
                                  fontSize: 14,
                                  color: Colors.white,
                                  fontWeight: FontWeight.w600,
                                  height: 22/14
                              )
                          ),
                        ),
                      ],
                    ),
                  ),
                  style: FilledButton.styleFrom(
                      backgroundColor: Color(0xFF48B2E7),
                      disabledBackgroundColor: Color(0xFF48B2E7),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(12)
                      )
                  ),
                ),)
              ],
            )
          ]
          )
      ),)
    ):Center(child: CircularProgressIndicator(),);
  }
}
