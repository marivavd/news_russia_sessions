import 'dart:typed_data';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localization/flutter_localization.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:image_picker/image_picker.dart';
import 'package:new_sessions/data/repository/request.dart';
import 'package:new_sessions/domain/avatar_use_case.dart';
import 'package:new_sessions/domain/edit_profile_use_case.dart';
import 'package:new_sessions/presentation/localization.dart';
import 'package:new_sessions/presentation/pages/forgot_pass.dart';
import 'package:new_sessions/presentation/pages/profile_yes.dart';
import 'package:new_sessions/presentation/utils/dialogs.dart';
import 'package:new_sessions/presentation/widgets/custom_field.dart';
import 'package:yandex_mapkit/yandex_mapkit.dart';

class ProfileEdit extends StatefulWidget {
  const ProfileEdit({super.key});

  @override
  State<ProfileEdit> createState() => _ProfileEditState();
}

class _ProfileEditState extends State<ProfileEdit> {
  var name = TextEditingController();
  var email = TextEditingController();
  var password = TextEditingController();
  EditProfileUseCase useCase = EditProfileUseCase();
  late AvatarUseCase avatarUseCase;
  bool flag = false;
  var avatar;


  void onPickAvatar(Uint8List bytes){
    setState(() {
     avatar = bytes;
    });
  }

  void onRemoveAvatar() {
    setState(() {
      avatar = null;
    });
  }

  Future<ImageSource?> onChooseSource()async{
    ImageSource? source;
    await showDialog(
        context: context,
        builder: (_) => AlertDialog(
          title: Text('Choose source'),
          actions: [
            TextButton(onPressed: (){
              source = ImageSource.camera;
              Navigator.pop(context);
            }, child: const Text("Camera")),
            TextButton(onPressed: (){
              source = ImageSource.gallery;
              Navigator.pop(context);
            }, child: const Text("Gallery"))
          ],

        )
    );
    return source;

  }


    @override
    void initState() {
      // TODO: implement initState
      super.initState();
      WidgetsBinding.instance.addPostFrameCallback((timeStamp) async {
        await useCase.getAvatar(
                (response){
                  avatar = response;
                },
                (String error)async{
                  showError(context, error);
                }
        );
        await useCase.getProfileItems(
                (response) {
              name.text = response[0];
              email.text = response[1];
              password.text = response[2];
              flag = true;
              setState(() {

              });
            },
                (String error) async {
              showError(context, error);
            }
        );
      });
      avatarUseCase = AvatarUseCase(onChooseSource: onChooseSource, onPickAvatar: onPickAvatar, onRemoveAvatar: onRemoveAvatar);
      setState(() {

      });
    }
    @override
    Widget build(BuildContext context) {
      return (flag) ? Scaffold(

        body: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: 48,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Align(
                        alignment: Alignment.topLeft,
                        child: InkWell(
                          child: Container(
                              height: 44,
                              width: 44,
                              decoration: BoxDecoration(
                                  shape: BoxShape.rectangle,
                                  borderRadius: BorderRadius.circular(40),
                                  color: Colors.white
                              ),
                              child: Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 19.25, vertical: 16.25),
                                child: SvgPicture.asset(
                                  'assets/back.svg', width: 5.5, height: 11.5,),
                              )
                          ),
                          onTap: () {
                            //Navigator.of(context).pop();
                          },
                        )
                    ),
                    Center(
                        child:
                        Align(
                          alignment: Alignment.topCenter,
                          child: Padding(
                            padding: EdgeInsets.only(top: 6),
                            child: Text(
                                AppLocale.profile.getString(context),
                                style: GoogleFonts.raleway(
                                    textStyle: TextStyle(
                                        fontWeight: FontWeight.w600,
                                        color: Color(0xFF2B2B2B),
                                        height: 20 / 16,
                                        fontSize: 16)
                                )
                            ),
                          ),
                        )

                    ),
                    Container(
                      height: 44,
                      width: 44,

                    ),

                  ],
                ),
                SizedBox(height: 40,),
                Align(
                  alignment: Alignment.center,
                  child: Container(
                    height: 80,
                    width: 80,
                    decoration: BoxDecoration(
                        border: Border.all(color: Colors.transparent, width: 0),

                        //image: DecorationImage(
                            //image:(avatar == null)?AssetImage('assets/avatar.png'):Image.memory(avatar!),
                           // fit: BoxFit.cover)
                    ),
                    child: Stack(
                      children: [
                    ClipOval(
                      child: (avatar != null)?Container(
                        decoration: BoxDecoration(
                            border: Border.all(color: Colors.transparent, width: 0),
                            borderRadius: BorderRadius.circular(40)),
                        child: Image.memory(avatar!, fit: BoxFit.cover, height: 80, width: 80,),
                      ):Image.asset('assets/avatar.png', fit: BoxFit.cover,),
                    ),

                        Padding(
                            padding: EdgeInsets.only(top: 63, left: 54),
                            child: InkWell(
                              child: Container(
                                height: 19,
                                width: 19,
                                decoration: BoxDecoration(
                                    border: Border.all(color: Color(0xFFF7F7F9)),
                                    color: Color(0xFF48B2E7),
                                    shape: BoxShape.circle

                                ),
                                child: SvgPicture.asset('assets/edit_pen.svg'),
                              ),
                              onTap: () async{
                                 avatarUseCase.pressButton(((avatar != null) ? true : false));
                              },
                            )
                        ),
                      ],
                    )

                  ),
                ),
                SizedBox(height: 22,),
                CustomField(label: AppLocale.name.getString(context),
                    hint: 'xxxxxxxx',
                    controller: name),
                SizedBox(height: 12,),
                CustomField(label: AppLocale.email.getString(context),
                    hint: 'xyz@gmail.com',
                    controller: email),
                SizedBox(height: 30,),
                CustomField(label: AppLocale.password.getString(context),
                  hint: '••••••••',
                  controller: password,
                  enableObscure: true,),
                SizedBox(height: 8,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    InkWell(
                      child: Text(
                        AppLocale.recoveryPass.getString(context),
                        style: GoogleFonts.raleway(
                            textStyle: TextStyle(
                                fontSize: 12,
                                color: Color(0xFF707B81),
                                height: 16 / 12
                            )
                        ),
                      ),
                      onTap: () {
                        Navigator.push(context, MaterialPageRoute(builder: (
                            context) => ForgotPass()));
                      },
                    )
                  ],
                ),
                SizedBox(height: 30,),
                Align(
                  alignment: Alignment.center,
                  child: SizedBox(
                    height: 50,
                    width: double.infinity,
                    child: FilledButton(
                      onPressed: () async {
                        showLoading(context);
                        await useCase.pressButton(name.text, email.text,
                            password.text,
                                (_) {
                          useCase.loadAvatar(avatar,
                                  (p0){
                                    hideLoading(context);
                                    Navigator.push(context, MaterialPageRoute(
                                        builder: (context) => Profile(fullname: name.text, avatar: avatar,)));
                                  },
                                  (String error) async {
                                hideLoading(context);
                                showError(context, error);
                              }
                          );

                            },
                                (String error) async {
                              hideLoading(context);
                              showError(context, error);
                            }
                        );
                      },
                      child: Text(
                        AppLocale.load.getString(context),
                        style: GoogleFonts.raleway(
                            textStyle: TextStyle(
                                fontSize: 14,
                                color: Colors.white,
                                fontWeight: FontWeight.w600,
                                height: 22 / 14
                            )
                        ),
                      ),
                      style: FilledButton.styleFrom(
                          backgroundColor: Color(0xFF48B2E7),
                          disabledBackgroundColor: Color(0xFF48B2E7),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(13)
                          )
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ) : Center(child: CircularProgressIndicator(),);
    }
  }