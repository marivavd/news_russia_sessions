import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';

class ConstantField extends StatefulWidget{
  final String label;
  final String text;

  const ConstantField({super.key, required this.label, required this.text

  });

  @override
  State<ConstantField> createState() => _ConstantFieldState();
}

class _ConstantFieldState extends State<ConstantField> {

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          widget.label,
          style: GoogleFonts.raleway(
              textStyle: TextStyle(
                  fontSize: 16,
                  color: Color(0xFF2B2B2B),
                  fontWeight: FontWeight.w500,
                  height: 20/16
              )
          ),
        ),
        const SizedBox(height: 17,),
        Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(14),
              border: Border.all(color: Colors.transparent),
              color: Color(0xFFF7F7F9)
          ),
          child: SizedBox(
            height: 48,
            width: double.infinity,
            child: Padding(
              padding: EdgeInsets.only(top: 13, bottom: 14, left: 16, right: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    widget.text,
                    style: GoogleFonts.poppins(
                      textStyle: TextStyle(
                        fontSize: 14,
                        height: 16/14,
                        color: Color(0xFF2B2B2B)
                      )
                    ),
                  ),
                  SvgPicture.asset('assets/yes.svg')
                ],
              ),
            ),
            // child: TextField(
            //   readOnly: true,
            //   controller: co,
            //   onChanged: widget.onChange,
            //   style:  GoogleFonts.raleway(
            //       textStyle: TextStyle(
            //           fontSize: 14,
            //           height: 16/14
            //       )
            //   ),
            //   decoration: InputDecoration(
            //       hintText: widget.hint,
            //       border: InputBorder.none,
            //       hintStyle: GoogleFonts.raleway(
            //           textStyle: TextStyle(
            //               fontSize: 14,
            //               color: Color(0xFF6A6A6A),
            //               height: 16/14
            //           )
            //       ),
            //       contentPadding: const EdgeInsets.symmetric(vertical: 10, horizontal: 14),
            //       suffixIconConstraints: const BoxConstraints(minWidth: 34),
            //       suffixIcon: (widget.enableObscure)?
            //       GestureDetector(
            //         onTap: (){
            //
            //           setState(() {
            //             isObscure = !isObscure;
            //           });
            //         },
            //         child: SvgPicture.asset(
            //             'assets/yes.svg'
            //           // colorFilter: ColorFilter.mode(colors.iconTint, BlendMode.color)
            //         ),
            //       )
            //           :null
            //
            //   ),
            // ),
          ),
        )
      ],
    );
  }

}
