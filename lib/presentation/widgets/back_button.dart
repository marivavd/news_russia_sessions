import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class BackButton extends StatelessWidget {
  const BackButton({super.key});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Container(
          height: 44,
          width: 44,
          decoration: BoxDecoration(
              shape: BoxShape.rectangle,
              borderRadius: BorderRadius.circular(40),
              color: Colors.white
          ),
          child: Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: 19.25, vertical: 16.25),
              child: InkWell(
                child: SvgPicture.asset(
                  'assets/back.svg', width: 5.5, height: 11.5,),
                onTap: (){
                  Navigator.of(context).pop();
                },
              )
          )
      ),
      onTap: () {
        Navigator.of(context).pop();
      },
    );
  }
}
