import 'package:flutter/material.dart';

class GenerationPasswordDialog extends StatelessWidget {
  const GenerationPasswordDialog({super.key, required this.onConfirm});

  final Function(String) onConfirm;

  @override
  Widget build(BuildContext context) {

    TextEditingController textEditingController = TextEditingController();

    return Dialog(
      child: PopScope(
          canPop: true,
          child: Padding(
            padding: const EdgeInsets.all(22.0),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                TextField(
                  controller: textEditingController,
                  decoration: const InputDecoration(
                      hintText: "Введите фразу"
                  ),
                ),
                const SizedBox(height: 38),
                TextButton(onPressed: (){
                  onConfirm(textEditingController.text);
                }, child: const Text("Сгенерировать пароль"))
              ],
            ),
          )
      ),
    );
  }
}
