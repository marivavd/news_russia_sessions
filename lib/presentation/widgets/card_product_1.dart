import 'dart:ui';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:new_sessions/data/models/model_product.dart';
import 'package:new_sessions/presentation/pages/details.dart';

class CardItem1 extends StatefulWidget {
  final ModelProduct product;


  const CardItem1({super.key, required this.product});

  @override
  State<CardItem1> createState() => _CardItem1State();
}

class _CardItem1State extends State<CardItem1> {
  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(16)
        ),
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [Stack(
              children: [
                Padding(padding: EdgeInsets.only(left: 9, top: 3),
                  child: Container(
                    height: 28,
                    width: 28,
                    decoration: BoxDecoration(
                        color: Color(0xFFF7F7F9),
                        borderRadius: BorderRadius.circular(40)
                    ),
                    child: Padding(
                      padding: EdgeInsets.all(6),
                      child: SvgPicture.asset('assets/heart.svg'),
                    ),
                  ),),Padding(padding: EdgeInsets.only(top: 34.54, left: 21, right: 21),
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        InkWell(

                          child: Container(
                            child: AspectRatio(
                              aspectRatio: 1,
                              child: CachedNetworkImage(
                                width: double.maxFinite,
                                fit: BoxFit.cover,
                                imageUrl: 'https://muozgansistgyudgofqc.supabase.co/storage/v1/object/public/ProductImages/60f2ced6-7b81-465d-bc0f-9c3395b78c56--999999.jpg',),),
                            ),


                          onTap: (){
                            Navigator.push(context, MaterialPageRoute(builder: (context) => Details(product: widget.product)));
                          },
                        ),
                      ] ),),

              ],
            ),
              Padding(padding: EdgeInsets.only(left: 9),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [

                    SizedBox(height: 12.6,),

                    Text("Best Seller".toUpperCase(), style: Theme.of(context).textTheme.labelMedium!.copyWith(color: Color(0xFF48B2E7), height: 16/12)),
                    const SizedBox(height: 8),
                    Text(widget.product.title, style: Theme.of(context).textTheme.titleMedium?.copyWith(height: 20/16), maxLines: 1,),
                    Padding(padding: EdgeInsets.only(top: 3.36),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Stack(
                            children: [
                              Padding(padding: EdgeInsets.only(left: 117),
                                child: Container(
                                  width: 34,
                                  height: 35.5,
                                  padding: const EdgeInsets.all(10),
                                  decoration: const BoxDecoration(
                                      color: Color(0xFF48B2E7),
                                      borderRadius: BorderRadius.only(
                                          topLeft: Radius.circular(14),
                                          bottomRight: Radius.circular(14)
                                      )
                                  ),
                                  child: SvgPicture.asset("assets/korsina.svg"),
                                ),
                              ),
                              Container(
                                height: 35.5,
                                alignment: Alignment.center,
                                child: Text("₽${widget.product.cost.toStringAsFixed(2)}",
                                  style: Theme.of(context).textTheme.labelSmall?.copyWith(height: 16/14, fontSize: 14),
                                ),
                              )


                            ],
                          )





                        ],
                      ),)



                  ],
                ),)
            ])

    );
  }
}
