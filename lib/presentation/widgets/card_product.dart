import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:new_sessions/data/models/model_product.dart';
import 'package:new_sessions/presentation/pages/details.dart';

class CardItem extends StatefulWidget {
  final ModelProduct product;
  bool isBasket;
  bool isFavourite;
  Future<void> Function() ontapFavourite;
  Future<void> Function() onTapBasket;


  CardItem({super.key, required this.product, required this.isFavourite, required this.isBasket, required this.onTapBasket, required this.ontapFavourite});

  @override
  State<CardItem> createState() => _CardItemState();
}

class _CardItemState extends State<CardItem> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 184,
      width: 160,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(16)
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
            children: [Stack(
        children: [
          Padding(padding: EdgeInsets.only(left: 9, top: 3),
          child: InkWell(
            child:  Container(
              height: 28,
              width: 28,
              decoration: BoxDecoration(
                  color: Color(0xFFF7F7F9),
                  borderRadius: BorderRadius.circular(40)
              ),
              child: Padding(
                  padding: EdgeInsets.all(6),
                  child: SizedBox(
                    width: 12,
                    height: 10.67,
                    child:  SvgPicture.asset((widget.isFavourite)?'assets/heart.svg':'assets/heart_no.svg'),
                  )
              ),
            ),
            onTap: widget.ontapFavourite,
          )
          ),
          Padding(padding: EdgeInsets.only(top: 34.54, left: 21),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  InkWell(

                    child: Container(
                      height: 54,
                      width: 115,
                      child: CachedNetworkImage(
                        width: double.maxFinite,
                        fit: BoxFit.cover,
                        imageUrl: widget.product.covers.first,),

                    ),
                    onTap: (){
                      Navigator.push(context, MaterialPageRoute(builder: (context) => Details(product: widget.product)));
                    },
                  ),
         ] ),),

        ],
      ),
      Padding(padding: EdgeInsets.only(left: 9),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.end,
          children: [

            SizedBox(height: 12.6,),

            Text("Best Seller".toUpperCase(), style: Theme.of(context).textTheme.labelMedium!.copyWith(color: Color(0xFF48B2E7), height: 16/12)),
            const SizedBox(height: 8),
            Text(widget.product.title, style: Theme.of(context).textTheme.titleMedium?.copyWith(height: 20/16), maxLines: 1,),
            Padding(padding: EdgeInsets.only(top: 3.36),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Stack(
                  children: [
                    Padding(padding: EdgeInsets.only(left: 117),
                      child: Container(
                        width: 34,
                        height: 35.5,
                        padding: const EdgeInsets.all(10),
                        decoration: const BoxDecoration(
                            color: Color(0xFF48B2E7),
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(14),
                                bottomRight: Radius.circular(14)
                            )
                        ),
                        child: InkWell(
                          child: (widget.isBasket)?SvgPicture.asset("assets/korsina.svg"):Text('+',

                            style: GoogleFonts.poppins(
                                textStyle: const TextStyle(
                                    fontWeight: FontWeight.w500,
                                    fontSize: 32,
                                    height: 16/32,
                                    color: Colors.white
                                )
                            ),),
                          onTap: widget.onTapBasket,
                        )
                      ),
                    ),
                      Container(
                        height: 35.5,
                        alignment: Alignment.center,
                        child: Text("₽${widget.product.cost.toStringAsFixed(2)}",
                          style: Theme.of(context).textTheme.labelSmall?.copyWith(height: 16/14, fontSize: 14),
                        ),
                      )


                  ],
                )





              ],
            ),)



          ],
        ),)
    ])

    );
  }
}
