import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_localization/flutter_localization.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:new_sessions/presentation/localization.dart';
import 'package:new_sessions/presentation/pages/home.dart';

Future<void> showOrderDialog(BuildContext context)async{
  await showDialog(context: context,
      builder: (_) => BackdropFilter(
        filter: ImageFilter.blur(sigmaX: 10, sigmaY: 10),
    child: Dialog(
        backgroundColor: Colors.white,

        child: Container(
          height: 375,
          width: 335,
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(20),
              border: Border.all(color: Colors.transparent)
          ),

          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(height: 40,),
              Container(
                height: 134,
                width: 134,
                decoration: BoxDecoration(
                    color: Color(0xFFDFEFFF),
                    borderRadius: BorderRadius.circular(200),
                    border: Border.all(color: Colors.transparent)
                ),
                child: Padding(
                    padding: EdgeInsets.all(24),
                    child: Image.asset('assets/image 50.png')
                ),
              ),
              SizedBox(height: 24,),
              Text(
                AppLocale.successful.getString(context),
                textAlign: TextAlign.center,
                style: GoogleFonts.raleway(
                    textStyle: TextStyle(
                        fontSize: 20,
                        color: Color(0xFF2B2B2B),
                        fontWeight: FontWeight.w500,
                        height: 56/40
                    )
                ),
              ),
              SizedBox(height: 30,),
              Align(
                alignment: Alignment.center,
                child: SizedBox(
                  height: 51,

                  width: 235,
                  child: FilledButton(
                    onPressed: (){
                      Navigator.push(context, MaterialPageRoute(builder: (context) => Home()));
                    },
                    child: Text(
                      AppLocale.backToShopping.getString(context),
                      style: GoogleFonts.raleway(
                          textStyle: TextStyle(
                              fontSize: 16,
                              color: Colors.white,
                              fontWeight: FontWeight.w500,
                              height: 19/16
                          )
                      ),
                    ),
                    style: FilledButton.styleFrom(
                        backgroundColor: Color(0xFF48B2E7),
                        disabledBackgroundColor: Color(0xFF48B2E7),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(16)
                        )
                    ),
                  ),
                ),
              ),
            ],
          ),
        )




    )),
  );
}