import 'dart:async';
import 'dart:io';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localization/flutter_localization.dart';
import 'package:new_sessions/data/repository/shared_preferences.dart';
import 'package:new_sessions/data/storage/queue.dart';
import 'package:new_sessions/presentation/localization.dart';
import 'package:new_sessions/presentation/pages/holder.dart';
import 'package:new_sessions/presentation/pages/map.dart';
import 'package:new_sessions/presentation/pages/onbording.dart';
import 'package:new_sessions/presentation/pages/sign_up.dart';
import 'package:new_sessions/presentation/utils/dialogs.dart';
import 'package:supabase_flutter/supabase_flutter.dart';
bool success = false;
Future<void> main() async{

  await Supabase.initialize(
    url: 'https://muozgansistgyudgofqc.supabase.co',
    anonKey: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6Im11b3pnYW5zaXN0Z3l1ZGdvZnFjIiwicm9sZSI6ImFub24iLCJpYXQiOjE3MTQ5ODgzNDcsImV4cCI6MjAzMDU2NDM0N30.7RdYIRhMPaOtQj4kcd1JYuhMQ-hrSgnlkXkA5TEZgk4',
  );
  WidgetsFlutterBinding.ensureInitialized();
  await initSharedPreferences();
  if (queue.getEndQueue()){
    success = true;
  }
  else{
  await queue.loadQueue();
  }
  runApp(const MyApp());
}
final supabase = Supabase.instance.client;

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> with WidgetsBindingObserver  {
  final connectivity = Connectivity();
  late StreamSubscription<List<ConnectivityResult>> connRes;
  final FlutterLocalization localization = FlutterLocalization.instance;


  @override
  void initState() {

    WidgetsBinding.instance.addObserver(this);
    // ru_RU -> ru
    final String localeName = Platform.localeName.split("_").first;
    localization.init(
      mapLocales: [
        const MapLocale('en', AppLocale.EN),
        const MapLocale('ru', AppLocale.RU),
      ],
      initLanguageCode: (localeName != "ru") ? "en" : "ru",
    );
    localization.onTranslatedLanguage = (_) => setState(() {});
    super.initState();


    connRes = connectivity.onConnectivityChanged.listen((event) {
      if (event == ConnectivityResult.none){

        showError(context, 'Internet crached');
      }
    });
  }
  @override
  void didChangeLocales(List<Locale>? locales) {
    if (locales == null){
      return;
    }
    localization.translate((locales.first.languageCode != "ru") ? "en" : "ru");
    super.didChangeLocales(locales);
  }

  @override
  void dispose() {
    // TODO: implement dispose
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
    connRes.cancel();
  }
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        supportedLocales: localization.supportedLocales,
        localizationsDelegates: localization.localizationsDelegates,
        title: 'Flutter Demo',
        home: //(success)?SignUp():OnBoarding()
Holder()

    );
  }
}
