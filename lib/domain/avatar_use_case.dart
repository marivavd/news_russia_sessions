import 'dart:typed_data';

import 'package:image_picker/image_picker.dart';

class AvatarUseCase{
  XFile? file;
  final Future<ImageSource?> Function() onChooseSource;
  final Function(Uint8List) onPickAvatar;
  final Function() onRemoveAvatar;

  AvatarUseCase(
      {
        required this.onChooseSource,
        required this.onPickAvatar,
        required this.onRemoveAvatar
      }
      );

  void pressButton(flag){
    if (flag){
      pressRemoveButton();
      flag = false;
    }else{

      pressChangeButton();
      flag = true;
    }
  }

  Future<void> pressChangeButton() async {
    var imageSource = await onChooseSource();
    if (imageSource == null){
      return;
    }
    file = (await ImagePicker().pickImage(source: imageSource));
    var bytes = await file?.readAsBytes();
    if (bytes != null) {
      onPickAvatar(bytes);

    }
  }

  void pressRemoveButton(){
    file = null;
    onRemoveAvatar();


  }

}