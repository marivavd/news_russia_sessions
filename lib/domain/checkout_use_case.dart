import 'package:new_sessions/data/repository/request.dart';
import 'package:new_sessions/domain/utils.dart';

class CheckoutUseCase {
  Future<void> emailandPhone(
      Function(List<String>) onResponse, Future<void> Function(String) onError) async {

        requestGetEmailandPhone() async {
          return await getParamentrs();
        }
        await requests(requestGetEmailandPhone, onResponse, onError);


  }

  Future<void> loadParametrsUser(String email, String phone, String cardName, String cardNumber,
      Function(void) onResponse, Future<void> Function(String) onError) async {
    if (email == '' || phone == '' || cardName == '' || cardNumber == ''){
      onError('Give all parametrs');
      return;
    }

    requestloadEmailandPhone() async {
      await loadAllParametrs(email, phone, cardName, cardNumber);
    }
    await requests(requestloadEmailandPhone, onResponse, onError);
  }

  Future<void> makeWholeOrder(String email, String phone, String cardName, String cardNumber, address,
      Function(int) onResponse, Future<void> Function(String) onError) async {
    requestmakeOrder() async {
      return await makeOrder(email, phone, cardName, cardNumber, address);
    }
    await requests(requestmakeOrder, onResponse, onError);
  }
  Future<void> makeWholeOrderItems(String title, double coast, int count, int idOrder, String idProduct,
      Function(void) onResponse, Future<void> Function(String) onError) async {
    requestmakeOrderItems() async {
      await makeOrderItems(title, coast, count, idOrder, idProduct);
    }
    await requests(requestmakeOrderItems, onResponse, onError);
  }
  Future<void> clearWholeBasket(
      Function(void) onResponse, Future<void> Function(String) onError) async {
    requestClear() async {
      await clearBasket();
    }
      await requests(requestClear, onResponse, onError);
    }

}