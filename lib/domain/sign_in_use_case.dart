import 'package:new_sessions/data/repository/request.dart';
import 'package:new_sessions/domain/utils.dart';

class SignInUseCase{
  Future<void> pressButton(String email, String password, Function(void) onResponse, Future<void> Function(String) onError)async{
    if (!checkEmail(email)){
      onError('Incorrect email');
    }
    else {
      if (password.isEmpty){
        onError('Password is empty');
      }
      else{
        requestSignIn()async{
          await signIn(email, password);
        }
        await requests(requestSignIn,onResponse, onError);
      }
    }
  }
  Future<void> pressButtonGoogle(
      Function(void) onResponse,
      Future<void> Function(String) onError
      ) async {
    requestSignInWithGoogle()async{
      await signInWithGoogle();
    }
    await requests(requestSignInWithGoogle, onResponse, onError);
  }



}
