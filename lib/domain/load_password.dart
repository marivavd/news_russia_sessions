import 'package:new_sessions/data/repository/shared_preferences.dart';

String getLoadingPassword(){
  return getPassword();
}
Future<void> loadPassword(String password)async{
  await setPassword(password);
}