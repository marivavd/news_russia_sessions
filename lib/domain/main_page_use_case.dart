import 'package:new_sessions/data/models/model_category.dart';
import 'package:new_sessions/data/repository/request.dart';
import 'package:new_sessions/domain/utils.dart';

class MainPageUseCase{
  Future<void> makeModelsCategories(Function(dynamic) onResponse, Future<void> Function(String) onError)async{
  getAllCatRequest()async{
    return await getAllCategories();
  }
  await requests(getAllCatRequest,onResponse, onError);}

  Future<void> makeModelsProductsBest(Function(dynamic) onResponse, Future<void> Function(String) onError)async{
    getAllCatRequest()async{
      return await getPopularProducts();
    }
    await requests(getAllCatRequest,onResponse, onError);}

  Future<void> getAdvertisimet(Function(dynamic) onResponse, Future<void> Function(String) onError)async{
    getAdvertRequest()async{
      return await getImageAdvert();
    }
    await requests(getAdvertRequest,onResponse, onError);}

}
