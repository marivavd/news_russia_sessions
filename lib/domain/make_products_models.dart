import 'package:flutter/material.dart';
import 'package:new_sessions/data/models/model_product.dart';
import 'package:new_sessions/data/repository/request.dart';

Future<List<ModelProduct>> makeProductsModels(data)async{
  List<ModelProduct> result = [];
  for (var val in data){
    List<String> covers = [
    ];
    List<Color> colors = [];
    for (var color in val['colors']){
      final image = await getImageofProduct(val['id'], color.toString().substring(1));
      covers.add(image);
      String colorForm = color.replaceAll('#', '0xff');
      colors.add(Color(int.parse(colorForm)));


    }
    result.add(ModelProduct(id: val['id'], title: val['title'], categoryId: val['categoryId'], cost: double.parse(val['cost'].toString()), description: val['description'], covers: covers, isFavorite: false, colors: colors));
  }
  return result;
}