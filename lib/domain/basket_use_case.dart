import 'package:new_sessions/data/repository/request.dart';
import 'package:new_sessions/domain/utils.dart';

class BasketUseCase {
  Future<void> isInBasket(String idProduct, Function(dynamic) onResponse, Future<void> Function(String) onError) async {
        requestGetBasket() async {
          return await getBasked(idProduct);
        }
        await requests(requestGetBasket, onResponse, onError);

  }

  Future<void> putInBasket(String idProduct, Function(void) onResponse, Future<void> Function(String) onError) async {
    requestGetBasket() async {
      await putBasked(idProduct);
    }
    await requests(requestGetBasket, onResponse, onError);

  }

  Future<void> updateBasket(String idProduct, Function(void) onResponse, Future<void> Function(String) onError) async {
    requestGetBasket() async {
      await updateBasked(idProduct);
    }
    await requests(requestGetBasket, onResponse, onError);

  }
  Future<void> getAllCart(Function(dynamic) onResponse, Future<void> Function(String) onError) async {
    requestGetBasket() async {
      return await getAllProductsInBasket();
    }
    await requests(requestGetBasket, onResponse, onError);

  }
}