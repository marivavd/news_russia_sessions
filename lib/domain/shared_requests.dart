import 'package:new_sessions/data/repository/shared_preferences.dart';

List<String> getLastRequests(){
  return getRequests();
}

Future<void> updateRequests(String request)async{
  await setRequests(request);
}