import 'package:new_sessions/data/models/model_product.dart';

List<ModelProduct> searchResults(String text, List<ModelProduct> products){
  List<ModelProduct> results = [];
  for (var val in products){
    if (val.title.toLowerCase().contains(text.toLowerCase())){
      results.add(val);
    }
  }
  return results;
}