import 'package:new_sessions/data/repository/request.dart';
import 'package:new_sessions/domain/utils.dart';

class EditProfileUseCase{
  Future<void> pressButton(String name, String email, String password, Function(void) onResponse, Future<void> Function(String) onError)async{
    if (!checkEmail(email)){
      onError('Incorrect email');
    }
    else {
      if (password.isEmpty){
        onError('Password is empty');
      }
      else{
        requestEdit()async{
          await editProfile(name, email, password);
        }
        await requests(requestEdit,onResponse, onError);
      }
    }
  }

  Future<void> getProfileItems(Function(dynamic) onResponse, Future<void> Function(String) onError)async{
        requestEdit()async{
          return await getProfile();
        }
        await requests(requestEdit,onResponse, onError);

  }

  Future<void> getPhoneNumber(Function(dynamic) onResponse, Future<void> Function(String) onError)async{
    requestEdit()async{
      return await getPhoneAndId();
    }
    await requests(requestEdit,onResponse, onError);

  }

  Future<void> loadAvatar(avatar, Function(dynamic) onResponse, Future<void> Function(String) onError)async{
    if(avatar == null){
      requestEdit()async{
        await removeAvatar();}
      await requests(requestEdit,onResponse, onError);
    }
    else{
      requestEdit()async{
        await uploadAvatar(avatar);}
      await requests(requestEdit,onResponse, onError);
    }

  }
  Future<void> getAvatar(Function(dynamic) onResponse, Future<void> Function(String) onError)async{


      requestEdit()async{
        return await getMyAvatar();}
      await requests(requestEdit,onResponse, onError);


  }




}
