import 'package:new_sessions/data/repository/request.dart';
import 'package:new_sessions/domain/utils.dart';

class FavouriteUseCase {
  Future<void> isInFavourite(String idProduct, Function(dynamic) onResponse, Future<void> Function(String) onError) async {
    requestGetFavourite() async {
      return await getFavourite(idProduct);
    }
    await requests(requestGetFavourite, onResponse, onError);

  }
  Future<void> putInFavourite(String idProduct, Function(dynamic) onResponse, Future<void> Function(String) onError) async {
    requestGetFavourite() async {
      return await putFavourite(idProduct);
    }
    await requests(requestGetFavourite, onResponse, onError);

  }
  Future<void> delFavourite(String idProduct, Function(dynamic) onResponse, Future<void> Function(String) onError) async {
    requestDelFavourite() async {
      await deleteFavourite(idProduct);
    }
    await requests(requestDelFavourite, onResponse, onError);

  }


  Future<void> getAllFavourites(Function(dynamic) onResponse, Future<void> Function(String) onError) async {
    requestGetFavourite() async {
      return await getAllFavouriteProduucts();
    }
    await requests(requestGetFavourite, onResponse, onError);

  }
}