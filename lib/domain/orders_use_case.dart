import 'package:new_sessions/data/repository/request.dart';
import 'package:new_sessions/domain/utils.dart';

class OrdersUseCase{
  Future<void> getIdOrders(Function(dynamic) onResponse, Future<void> Function(String) onError)async{
    getOrderRequest()async{

        return await getOrders();

    }
    await requests(getOrderRequest,onResponse, onError);}

  Future<void> getOrdersItems(int idOrder, Function(dynamic) onResponse, Future<void> Function(String) onError)async{
    getOrderRequest()async{

      return await getOrderItems(idOrder);

    }
    await requests(getOrderRequest,onResponse, onError);}

}
