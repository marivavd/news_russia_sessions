import 'package:new_sessions/data/repository/request.dart';
import 'package:new_sessions/domain/utils.dart';

class SendCodeUseCase{
  Future<void> pressButton(String email, Function(void) onResponse, Future<void> Function(String) onError)async{
    if (!checkEmail(email)){
      onError('Incorrect email');
    }
    else {
        requestSendCode()async{
          await sendCode(email);
        }
        await requests(requestSendCode,onResponse, onError);

    }
  }




}
