import 'package:new_sessions/data/models/model_product.dart';

void countProducts(List<ModelProduct> result, List<Map<String, dynamic>> data){
  for (int i = 0; i < result.length; i ++){
    result[i].count = data[i]['count'];
  }
}

int getCountProducts(List<ModelProduct> result){
  int count = 0;
  for (int i = 0; i < result.length; i ++){
    count += result[i].count;
  }
  return count;
}

double getSummaProducts(List<ModelProduct> result){
  double summa = 0;
  for (int i = 0; i < result.length; i ++){
    summa += result[i].count * result[i].cost;
  }
  return summa;
}