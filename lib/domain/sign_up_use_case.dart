import 'package:new_sessions/data/repository/request.dart';
import 'package:new_sessions/domain/utils.dart';

class SignUpUseCase{
  Future<void> pressButton(String name, String email, String password, Function(void) onResponse, Future<void> Function(String) onError)async{
    if (!checkEmail(email)){
      onError('Incorrect email');
    }
    else {
      if (password.isEmpty){
        onError('Password is empty');
      }
      else{
        requestSignUp()async{
          await signUp(name, email, password);
        }
          await requests(requestSignUp,onResponse, onError);
        }
      }
    }
  Future<void> pressButtonGoogle(
      Function(void) onResponse,
      Future<void> Function(String) onError
      ) async {
    requestSignInWithGoogle()async{
      await signInWithGoogle();
    }
    await requests(requestSignInWithGoogle, onResponse, onError);
  }



}
