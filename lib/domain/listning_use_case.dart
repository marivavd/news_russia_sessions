import 'package:new_sessions/data/repository/request.dart';
import 'package:new_sessions/domain/utils.dart';

class ListningUseCase{
  Future<void> makeModelsProducts(String idCategory, Function(dynamic) onResponse, Future<void> Function(String) onError)async{
    getProductsRequest()async{
      if (idCategory == ''){
        return await getProducts();
      }
      else{
        return await getProductsByCategory(idCategory);
      }
    }
    await requests(getProductsRequest,onResponse, onError);}

}
