import 'package:new_sessions/data/repository/request.dart';
import 'package:new_sessions/domain/utils.dart';

class SignOutUseCase{
  Future<void> pressButton(Function(void) onResponse, Future<void> Function(String) onError)async {
    requestSignIOut() async {
      await logOut();
    }
    await requests(requestSignIOut, onResponse, onError);
  }




}
