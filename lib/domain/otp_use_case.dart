import 'package:new_sessions/data/repository/request.dart';
import 'package:new_sessions/domain/utils.dart';

class OTPUseCase{
  Future<void> verify(
      String email,
      String code,
      Function(void) onResponse,
      Future<void> Function(String) onError) async {
    requestVerCode()async{
      await verifyCode(email, code);
    }
    await requests(
        requestVerCode,
        onResponse,
        onError
    );
  }

  Future<void> setNewPassword(
      String newPassword,
      Function(void) onResponse,
      Future<void> Function(String) onError
      ) async {
    requestSetNewPassword()async{
      changePass(newPassword);
    }
    await requests(
        requestSetNewPassword,
        onResponse,
        onError);
  }
  final _encodeMap = {
    "а": "/-\\",
    "б": "|o",
    "в": "v",
    "г": "g",
    "д": "9",
    "е": "e",
    "ё": "e",
    "ж": "8|8",
    "з": "z",
    "и": "|e",
    "й": "|e'",
    "к": "k",
    "л": "l",
    "м": "m",
    "н": "n",
    "о": "0",
    "п": "p",
    "р": "r",
    "с": "c",
    "т": "t",
    "у": "y",
    "ф": "f",
    "х": "x",
    "ц": "c,",
    "ч": "4",
    "ш": "sh",
    "щ": "sha",
    "ъ": "'b",
    "ы": "b|",
    "ь": "b",
    "э": "ia",
    "ю": "|0",
    "я": "I",
  };

  void preparePassword(
      String text,
      Function(String) onGood,
      Function(String) onBad
      ){
    var bufferPassword = generatePassword(text);
    if (bufferPassword.length < 8) {
      onBad("Фраза слишком кароткая");
    }else{
      onGood(bufferPassword);
    }
  }

  String generatePassword(String text){
    var result = "";
    for (int i=0; i < text.length; i++) {
      var element = text[i].toLowerCase();
      if (_encodeMap.containsKey(element)){
        var buf = _encodeMap[element]!;
        if (i % 2 == 0){
          buf = buf.toUpperCase();
        }
        result += buf;
      }else{
        result += element;
      }
    }
    return result;
  }


}